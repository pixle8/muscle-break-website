<!DOCTYPE html>
<html <?php language_attributes(); ?>>
	<head>
		<meta charset="<?php bloginfo( 'charset' ); ?>" />
		<meta name="viewport" content="width=device-width,initial-scale=1">
		<title><?php wp_title(''); ?></title>
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto:400,700" rel="stylesheet">
		<link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/styles.css">
		<link rel="stylesheet" href="<?php bloginfo('template_directory');?>/css/black-tie.min.css">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_uri(); ?>" />
		<?php wp_head(); ?>
	</head>
	<body <?php body_class(); ?>>
	<div id="fb-root"></div>
		<section id="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="logo-wrap">
							<a href="<?php echo get_home_url(); ?>"><img src="<?php bloginfo('template_directory');?>/images/logo.png" alt=""></a>
						</div>
					</div>
					<div class="col-md-10 nav-wrap hide-mobile">
						<ul class="nav-menu">
							<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'main-menu' ) ); ?>
							<li class="nav-item"><a id="search-trigger" href="#"><i class="btb bt-search"></i></a>
								<div id="search-wrap" class="search hide-mobile hidden">

									<form role="search" method="get" class="searchform" action="<?php echo home_url( '/' ); ?>">
									        <input type="search" class="search-field"
									            placeholder="<?php echo esc_attr_x( 'Search…', 'placeholder' ) ?>"
									            value="<?php echo get_search_query() ?>" name="s"
									            title="<?php echo esc_attr_x( 'Search for:', 'label' ) ?>" />				    
									         <button value="<?php echo esc_attr_x( 'Search', 'submit button' ) ?>"><i class="btb bt-search"></i></button>
									</form>
								</div>
							</li>
						</ul>
					</div>

					<div class="show-mobile col-md-2 float-right">
						<div class="button-wrap">
							<button class="hamburger">&#9776;</button>
							<button class="cross">&#735;</button>
						</div>
						<div class="menu">
						  <ul>
						    <?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'main-menu' ) ); ?>
						    <a href="#"><li><i class="btb bt-search"></i></li></a>
						  </ul>
						</div> 
					</div>
				</div>
			</div>
		</section>
		<!-- End top bar -->