<?php get_header(); ?>
<section id="content">
	<div class="container">
	<?php if ( have_posts() ) : ?>
		<h2 class="entry-title"><?php printf( __( 'Search Results for: %s', 'musclebreak' ), get_search_query() ); ?></h1>

			<div class="row">
				<div class="col-md-8">
					<div class="posts-list">
					<?php while ( have_posts() ) : the_post(); ?>
					<?php 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
						$thumb_url = $thumb_url_array[0];
					?>
						<article class="post">
							<a href="<?php the_permalink(); ?>" class="image"><img src="<?php echo $thumb_url; ?>" alt=""></a>
							<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							<p><?php echo excerpt(14); ?>...</p>
						</article>
					<?php endwhile; ?>
					</div>
					<div class="row text-center">
						<?php numeric_posts_nav(); ?>
					</div>
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>

	<?php else : ?>
		<div class="row">
			<div class="col-md-8">
				<header class="header">
					<h2 class="entry-title"><?php _e( 'Nothing Found', 'musclebreak' ); ?></h2>
				</header>
				<section class="entry-content">
					<p><?php _e( 'Sorry, nothing matched your search. Please try again.', 'musclebreak' ); ?></p>
					<?php get_search_form(); ?>
				</section>
			</div>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	<?php endif; ?>
	</div>
</section>
<?php get_footer(); ?>