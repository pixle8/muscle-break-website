$('.calculate-btn').click(function() {
	var system = $('input[name=system]:checked').val()
	if (system == 'imperial') {
		var weight = parseInt($('input[name=weightlbs]').val());
		var heightft = parseInt($('input[name=heightft]').val());
		var heightin = parseInt($('input[name=heightin]').val());
		var finalHeight = (heightft*12)+heightin;
		var bmi = Math.round((weight/(finalHeight*finalHeight))*703);
	}
	else {
		var weight = parseInt($('input[name=weightkg]').val());
		var heightcm = parseInt($('input[name=heightcm]').val());
		var heightm = heightcm*0.01;
		var bmi = Math.round((weight/(heightm*heightm)));
	}

	$('#calc_value').attr('value',bmi);
	$('.cal-result-val').text(bmi);


	$('#results').css('display', 'block');
});