// Hamburger Menu
$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
	$( ".menu" ).slideToggle( "slow", function() {
		$( ".hamburger" ).hide();
		$( ".cross" ).show();
	});
});

$( ".cross" ).click(function() {
	$( ".menu" ).slideToggle( "slow", function() {
		$( ".cross" ).hide();
		$( ".hamburger" ).show();
	});
});

function myPopup(url) {

	var w = 600;
	var h = 500;
	var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

    var width = window.innerWidth ? window.innerWidth : document.documentElement.clientWidth ? document.documentElement.clientWidth : screen.width;
    var height = window.innerHeight ? window.innerHeight : document.documentElement.clientHeight ? document.documentElement.clientHeight : screen.height;

    var left = ((width / 2) - (w / 2)) + dualScreenLeft;
    var top = ((height / 2) - (h / 2)) + dualScreenTop;

    window.open( url, "myWindow", "status = 1, height = 500, width = 600, resizable = 0, top=" + top + ', left=' + left );
}

// Search Hide/Show
$('#search-trigger').click(function() {
	if ($('#search-wrap').hasClass('hidden')) {
		$('#search-wrap').removeClass('hidden');
	}
	else {
		$('#search-wrap').addClass('hidden');
	}
});

var $tabs = $('.articles-wrap > ul');
$('.menu-posts a').on('click', function(e) {
	e.preventDefault();
	var currentTab = $(this).attr('href');

	$(this).parent().addClass('active').siblings().removeClass('active');
	$tabs.hide();
	$(currentTab).show();
}).filter(':first').click();

$('.unit-selection div').click(function() {
	$('.unit-selection div').removeClass('active');
	$(this).addClass('active');
	$(this).find('input').prop('checked',true);
	if ($('input[name=system]:checked').val() == 'imperial') {
		$('.age-height div.imperial').removeClass('hidden');
		$('.age-height div.metric').addClass('hidden');
		$('span.metric-max').addClass('hidden');
		$('span.imperial-max').removeClass('hidden');
		var handle = $( '.ui-slider-handle' );
		$( '#weight-slider' ).slider({
			max: 600,
			value: 200,
			create: function() {
				handle.html( $( this ).slider( 'value' ) + "<br><span>lbs</span>");
				$('#weight-input').val($( this ).slider( 'value' ));
			},
			slide: function( event, ui ) {
				handle.html( ui.value + "<br><span>lbs</span>" );
				$('#weight-input').val($( this ).slider( 'value' ));
			}
		});
	}
	else {
		$('.age-height div.metric').removeClass('hidden');
		$('.age-height div.imperial').addClass('hidden');
		$('span.imperial-max').addClass('hidden');
		$('span.metric-max').removeClass('hidden');
		var handle = $( '.ui-slider-handle' );
		$( '#weight-slider' ).slider({
			max: 400,
			value: 80,
			create: function() {
				handle.html( $( this ).slider( 'value' ) + "<br><span>kg</span>");
				$('#weight-input').val($( this ).slider( 'value' ));
			},
			slide: function( event, ui ) {
				handle.html( ui.value + "<br><span>kg</span>" );
				$('#weight-input').val($( this ).slider( 'value' ));
			}
		});
	}
});

$('div.gender .half div').click(function() {
	$(this).find('input').prop('checked',true);
});

$(document).ready(function() {
	var handle = $( '.ui-slider-handle' );
	$( '#weight-slider' ).slider({
		max: 600,
		value: 200,
		create: function() {
			handle.html( $( this ).slider( 'value' ) + "<br><span>lbs</span>");
			$('#weight-input').val($( this ).slider( 'value' ));
		},
		slide: function( event, ui ) {
			handle.html( ui.value + "<br><span>lbs</span>" );
			$('#weight-input').val($( this ).slider( 'value' ));
		}
	});
});
$( "#activity" ).selectmenu();


$('.man').click(function() {
	$(this).addClass('active');
	$('.woman').removeClass('active');
});

$('.woman').click(function() {
	$(this).addClass('active');
	$('.man').removeClass('active');
});

$('.imperial').click(function() {
	$(this).addClass('current');
	$('.metric').removeClass('current');
});

$('.metric').click(function() {
	$(this).addClass('current');
	$('.imperial').removeClass('current');
});
var request;
$('.email-submit').submit(function(event){
	event.preventDefault();

    if (request) {
        request.abort();
    }
    $("#result-top").html('Processing.....');
    var $form = $(this);
    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();

    console.log(serializedData);

    $inputs.prop("disabled", true);
    console.log('yes');
    // Fire off the request to /form.php
    request = $.ajax({
        url: ajaxurl,
        type: "POST",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        $("#result").html('Thanks for subscribing!');
        $("#sidebarresult").html('Thanks for subscribing!');
        $('input[name="email"]').val('');
        $("#result-top").html('Thanks!');
        $("#results").fadeIn(function () {
            $('#lightbox-cover').trigger('click');
            $("#result-top").html('');
            $('input[name="email"]').attr('value','');
            $('input[name="email"]').val('');
        });

        $('.calculator .results').fadeIn();
    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
		var response = JSON.parse(jqXHR.responseText);
        $("#result-top").html(response.errors.detail);
        $("#result").html(response.errors.detail);
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });
});

$('.bmi-calc-email-submit').submit(function(event){
	event.preventDefault();

    if (request) {
        request.abort();
    }
    var $form = $(this);
    var $inputs = $form.find("input, select, button, textarea");

    var serializedData = $form.serialize();

    $inputs.prop("disabled", true);

    // Fire off the request to /form.php
    request = $.ajax({
        url: templateDirectory + "/subscribe.php",
        type: "post",
        data: serializedData
    });

    // Callback handler that will be called on success
    request.done(function (response, textStatus, jqXHR){
        // Log a message to the console
        console.log('it worked');
        // $("#result").html('Your BMI is:' + bmi);
        $('#results').removeClass('hidden');

    });

    // Callback handler that will be called on failure
    request.fail(function (jqXHR, textStatus, errorThrown){
        // Log the error to the console
        console.error(
            "The following error occurred: "+
            textStatus, errorThrown
        );
    });

    // Callback handler that will be called regardless
    // if the request failed or succeeded
    request.always(function () {
        // Reenable the inputs
        $inputs.prop("disabled", false);
    });
});

$('#lightbox-cover').click(function() {
	$('#lightbox-opt, #lightbox-cover').addClass('hidden');
});

$('#back-to-top').click(function() {
	$('html, body').animate({ scrollTop: 0 }, 'slow');
});


(function (load_more) {

    // The global jQuery object is passed as a parameter
    load_more(window.jQuery, window, document);

}(function ($, window, document) {
    $(function () {

        $("#more-posts a").click(function (e) {
            e.preventDefault();
            var page = parseInt($(this).attr('data-page'));
            var category = parseInt($(this).attr('data-category'));
            var tag = $(this).attr('data-tag');
            var pageType = $(this).attr('data-page-type');
            var author = $(this).attr('data-author');
            var reference = $(this);
            $(this).parent().fadeOut('slow');
            $.ajax({
                type: "GET",
                url: ajaxurl,
                dataType: 'html',
                data: ({action: 'loadMore',page:page,category:category,tag:tag,author:author,pageType:pageType}),
                success: function (data) {
                    $('.posts-list').append(data);
                    $('.articles-list').append(data);
                    console.log(data);
                    page = page+1;
                    reference.attr('data-page',page);
                    reference.parent().fadeIn(function(){
                    
                    });
                    if (page > $('#max-pages').val()) {
                        $('#more-posts a').addClass('hidden');
                        $('#no-more-posts').removeClass('hidden');
                    }

                },
                error: function (jqXHR, exception) {
                    var msg = '';
                    console.log('problem');
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                },
            });
            return false;

        });

    });

}));











