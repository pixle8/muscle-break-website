$('.calculate-btn').click(function() {
	var system = $('input[name=system]:checked').val()
	if (system == 'imperial') {
		var gender = $('input[name=gender]:checked').val();
		var age = $('input[name=age]').val();
		var weight = $( "#weight-slider" ).slider( "value" );
		var heightft = parseInt($('input[name=heightft]').val());
		var heightin = parseInt($('input[name=heightin]').val());
		var finalHeight = (heightft*12)+heightin;
		var activity = parseFloat($('select[name=activity]').val());
		if (gender == 'male') {
			var bmr = (66 + (6.23 * weight) + (12.7*finalHeight) - (6.76 * age));
			bmr = Math.round((bmr*activity));
		}
		else {
			var bmr = (655 + (4.35 * weight) + (4.7*finalHeight) - (4.7 * age));
			bmr = Math.round((bmr*activity));
		}
	}
	else {
		var gender = $('input[name=gender]:checked').val();
		var age = $('input[name=age]').val();
		var weight = $( "#weight-slider" ).slider( "value" );
		var heightcm = parseInt($('input[name=heightcm]').val());
		var activity = parseFloat($('select[name=activity]').val());
		if (gender == 'male') {
			var bmr = (66 + (13.7 * weight) + (5*heightcm) - (6.76 * age));
			bmr = Math.round((bmr*activity));
		}
		else {
			var bmr = (655 + (9.6 * weight) + (1.8*heightcm) - (4.7 * age));
			bmr = Math.round((bmr*activity));
		}
	}

    $('#calc_value').attr('value',bmr);
    $('.cal-result-val').text(bmr);

    $('#results').removeClass('hidden');
    $('#results').css('display', 'block');

    // $('#lightbox-opt, #lightbox-cover').removeClass('hidden');

});