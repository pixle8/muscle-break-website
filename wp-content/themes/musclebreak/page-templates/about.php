<?php get_header(); 
// Template Name: About
?>

<section id="about-banner">
	<div class="container">
		<h2 class="page-title">about <strong>musclebreak</strong></h2>
	</div>
</section>
<!-- End About Banner -->

<section id="mission">
	<div class="container">
		<h2 class="back-title">MISSION</h2>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<p>This is a short mission statement that will be changed. There’s no one way to be healthy. What’s good for you is what you’ll stick with, and you’ll stick with what you love. <span>Our mission is to spread this “healthy attitude” around the world.</span></p>
				<p>Today, we do that by helping you find what’s good for you. Not like "eat your vegetables, they're good for you." More like “here are some choices you can realistically make, stick with, and feel really good about.” Because in the end, you don't have to choose between being happy and being healthy; they're really the same thing.</p>
			</div>
		</div>
	</div>
</section>
<!-- End Mission -->

<section id="we-are">
	<div class="container">
		<h3>We Are...</h3>

		<div class="text-wrap">
			<div class="row">
				<div class="col-sm-6">
					<h2>Dedicated</h2>
					<p>We're just as committed to results as you are.</p>
				</div>
				<div class="col-sm-6">
					<h2>Honest</h2>
					<p>We’re here for you, not clickbait.</p>
				</div>
			</div>

			<div class="row">
				<div class="col-sm-6">
					<h2>Authentic</h2>
					<p>No false promises, no bullshit. Real articles</p>
				</div>
				<div class="col-sm-6">
					<h2>Gym Buddies</h2>
					<p>We’re the people in the gym with you, day in and day out.</p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End We Are -->

<section id="team">
	<div class="container">
		<h2>THE <strong>Dream Team</strong></h2>
		<ul class="members">
			<li>
				<div class="member" style="background-image: url('<?php bloginfo('template_directory');?>/images/glenn.jpg');">
					<div class="caption">
						<h4>Glenn Francis</h4>
						<p>Founder</p>
					</div>
				</div>
			</li>
			<?php $blogusers = get_users( 'blog_id=1&orderby=nicename&role=contributor' );
			// Array of WP_User objects.
			foreach ( $blogusers as $user ) {
				$user_id = esc_html( $user->ID);				
				$image = get_field('about_page_image', 'user_' . $user_id); ?>
				<li>
					<div class="member" style="background-image: url('<?php echo $image; ?>');">
						<div class="caption">
							<h4><?php echo esc_html( $user->display_name); ?></h4>
							<p>Contributor</p>
						</div>
					</div>
				</li>
			<?php } ?>
		</ul>
	</div>
</section>
<!-- End Team -->

<?php get_footer(); ?>