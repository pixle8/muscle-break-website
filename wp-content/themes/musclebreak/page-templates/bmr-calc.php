<?php get_header(); 
// Template Name: BMR Calculator
?>
	<section id="bmr-banner" class="banner">
		<div class="container">
			<h2 class="page-title"><strong>BMR Calculator</strong></h2>
		</div>
	</section>
	<!-- End About Banner -->
	<section class="calculator-content" id="bmr-calc">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="text">
						<h4><strong>Basal metabolic rate (BMR)</strong> is the amount of energy expended while at rest in a neutrally temperate environment. Find out your BMR with this handy calculator!</h4>
						<p>
							Basal metabolic rate (BMR) is the amount of energy expended while at rest in a neutrally temperate environment, in the post-absorptive state (meaning that the digestive system is inactive, which requires about twelve hours of fasting).
						</p>
						<p>
							The release of energy in this state is sufficient only for the functioning of the vital organs, such as the heart, lungs, brain and the rest of the nervous system, liver, kidneys, sex organs, muscles and skin. BMR decreases with age and with the loss of lean body mass. Increasing muscle mass increases BMR.
						</p>
					</div>
				</div>
				<div class="col-md-7">
					<div class="calculator  full-left">
						<div class="units">
							<div class="half pull-left">
								<p>Choose System:</p>
							</div>
							<div class="half pull-right">
								<div class="unit-selection">
									<div class="imperial current">
										<input type="radio" name="system" value="imperial" checked="checked">
										Imperial
									</div>
									<div class="metric">
										<input type="radio" name="system" value="metric">
										Metric
									</div>
								</div>
							</div>
							<div class="clear-fix"></div>
						</div>
						<form>
							<!-- Gender -->
							<div class="gender">
								<label for="gender">Gender:</label>
								<div class="half pull-left">
									<div class="man select active">
										<div class="selection-circle"><div class="inner-selected"></div></div>
										Man
										<i class="btm bt-user-male"></i>
										<input type="radio" name="gender" value="male" checked>
									</div>
								</div>
								<div class="half pull-right">
									<div class="woman select">
										<div class="selection-circle"><div class="inner-selected"></div></div>
										Woman
										<i class="btm bt-user-female"></i>
										<input type="radio" name="gender" value="female">
									</div>
								</div>
							</div>
							<!-- End Gender Row -->
							<div class="age-height"> <!-- Age/Height -->
								<div class="half pull-left">
									<div class="age-wrap">
										<label for="age">
											Age:
										</label>
										<div class="input-wrap">
											<input type="number" name="age" value="18" placeholder="18">
											<span>years old</span>
										</div>
									</div>
								</div>
								<div class="half pull-right">
									<div class="height-wrap imperial">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="number" name="heightft" value="5" placeholder="5">
											<span>ft</span>
											<input type="number" name="heightin" value="10" placeholder="10">
											<span>in</span>
										</div>
									</div>
									<div class="height-wrap metric hidden">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="number" name="heightcm" placeholder="170"
											value="170" required="required">
											<span>cm's</span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Age/Height -->
							<div class="weight"> <!-- Weight -->
								<label for="weight">Weight:</label>
								<div class="slider weight-slider">
									<div id="weight-slider" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span></div>
									<span class="min"><span class="imperial-min">0</span> <span class="metric-min">0</span></span>
									<span class="max"><span class="imperial-max">600</span> <span class="metric-max hidden">227</span></span>
								</div>
								<input type="hidden" name="weight" id="weight-input">
							</div>
							<!-- End Weight -->
							<div class="activity"> <!-- Activity -->
								<label for="activity">Activity Level:</label>
								<div class="activity-wrap">
									<select name="activity" id="activity">
									  <option value="1.2">Sedentary</option>
									  <option value="1.375">Light Activity</option>
									  <option value="1.55" selected="selected">Moderate Activity</option>
									  <option value="1.725">Very Active</option>
									  <option value="1.9">Extra Active</option>
									</select>
								</div>
							</div>
							<!-- End Activity -->
						</form>
                        <div id="results" style="display: none;">
                            <p>Your BMR Is: <span class="cal-result-val"></span></p>
                        </div>
						<div class="submit full-left"> <!-- Submit -->
							<div class="calculate-btn">
								<div>Calculate My BMR</div>
							</div>
						</div>
						<!-- End Submit -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php bloginfo('template_directory');?>/js/bmr.js"></script>
    <div id="lightbox-opt" class="hidden">
        <h3>Where should we send your results?</h3>
        <p>Please enter your email below and we will send your results</p>
        <span id="result-top"></span>
        <form class="email-submit">
            <input type="email" name="email" placeholder="Your Email">
            <input type="submit" value="Send Me My Results!" class="bmi-calc-email-submit">
            <input type="hidden" name="action" value="add_user_to_active_campaign">
            <input type="hidden" name="bmr" value="0" id="calc_value">
        </form>
    </div>
    <div id="lightbox-cover" class="hidden"></div>
<?php get_footer(); ?>