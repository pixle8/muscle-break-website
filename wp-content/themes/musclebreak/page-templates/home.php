<?php get_header(); 
// Template Name: Home Page
?>

<?php $args = array('posts_per_page' => 1);
$query1 = new WP_Query( $args );
if ( $query1->have_posts() ) { while ( $query1->have_posts() ) { $query1->the_post(); ?>
	<?php
		$thumb_id = get_post_thumbnail_id();
		$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
		$thumb_url = $thumb_url_array[0];
	?>
	<section id="home-banner" style="background-image: url('<?php echo $thumb_url; ?>')">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="col-md-6 col-md-offset-3 text-center">
					<p><?php echo excerpt(14); ?></p>
				</div>
				<div class="col-md-4 col-md-offset-4 text-center">
					<a href="<?php the_permalink(); ?>" class="button red">Read More</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End Home-banner -->
<?php } wp_reset_query(); wp_reset_postdata(); } ?>

<section id="article-flow-1">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="articles">
					<div class="row">
						<?php $args2 = array('posts_per_page' => 1, 'offset' => 1);
						$query2 = new WP_Query( $args2 );
						if ( $query2->have_posts() ) { while ( $query2->have_posts() ) { $query2->the_post(); ?>
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
								$thumb_url = $thumb_url_array[0];
							?>
							<li class="col-md-3"><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
									<h2><?php the_title(); ?></h2>
								</div>
							</a></li>
						<?php } wp_reset_query(); wp_reset_postdata(); } ?>

						<?php $args3 = array('posts_per_page' => 1, 'offset' => 2);
						$query3 = new WP_Query( $args3 );
						if ( $query3->have_posts() ) { while ( $query3->have_posts() ) { $query3->the_post(); ?>
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
								$thumb_url = $thumb_url_array[0];
							?>
							<li class="col-md-6"><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
									<h2><?php the_title(); ?></h2>
								</div>
							</a></li>
						<?php } wp_reset_query(); wp_reset_postdata(); } ?>

						<li class="col-md-3">
							<ul class="secondary-list">
								<div class="row">
									<li class="facebook">
										<div class="text-wrap">
											<a href="https://www.facebook.com/musclebreak/" class="button" target="_blank">Like our page</a>
										</div>
									</li>
								</div>
								<div class="row">
									<li class="instagram">
										<div class="text-wrap">
											<a href="https://www.instagram.com/musclebreakcom/" class="button" target="_blank">Follow Us</a>
										</div>
									</li>
								</div>
							</ul>
						</li>
					</div>
					<div class="row">
						<?php $args4 = array('posts_per_page' => 1, 'offset' => 3);
						$query4 = new WP_Query( $args4 );
						if ( $query4->have_posts() ) { while ( $query4->have_posts() ) { $query4->the_post(); ?>
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
								$thumb_url = $thumb_url_array[0];
							?>
							<li class="col-md-6"><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
									<h2><?php the_title(); ?></h2>
									<!-- <p class="excerpt"><?php echo excerpt(14); ?></p> -->
								</div>
							</a></li>
						<?php } wp_reset_query(); wp_reset_postdata(); } ?>

						<li class="col-md-6">
							<ul class="secondary-list articles">
								<?php $args5 = array('posts_per_page' => 2, 'offset' => 4);
								$query5 = new WP_Query( $args5 );
								if ( $query5->have_posts() ) { while ( $query5->have_posts() ) { $query5->the_post(); ?>
									<?php
										$thumb_id = get_post_thumbnail_id();
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
										$thumb_url = $thumb_url_array[0];
									?>
									<div class="row">
										<li><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
												<h2><?php the_title(); ?></h2>
											</div>
										</a></li>
									</div>
								<?php } wp_reset_query(); wp_reset_postdata(); } ?>
							</ul>
						</li>
					</div>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- End Article Flow 1 -->
<section class="ad">
	<div class="container">
		<div class="row">
			<div class="col-md-8 ad-wrap text-center col-md-offset-2">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- AD2 -->
				<ins class="adsbygoogle"
     			style="display:inline-block;width:728px;height:90px"
     			data-ad-client="ca-pub-7118737131388553"
     			data-ad-slot="1478566822"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		</div>
	</div>
</section>
<!-- End Ad -->
<section id="trending">
	<div class="container">
		<div class="row text-center">
			<h2><i class="btm bt-bar-chart"></i>Trending Articles</h2>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<ul class="articles">
				<?php 
				$popularpost = new WP_Query( array( 'posts_per_page' => 4, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
				while ( $popularpost->have_posts() ) : $popularpost->the_post(); 
				$thumb_id = get_post_thumbnail_id();
				$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
				$thumb_url = $thumb_url_array[0];
				$title = get_the_title();
				?>
				<li class="article col-md-3"><a href="<?php the_permalink(); ?>">
					<div class="title-wrap">
						<h3><?php echo mb_strimwidth($title, 0, 42, '...'); ?></h3>
					</div>
					<div class="featured-image-block" style="background-image: url('<?php echo $thumb_url; ?>');"></div>
				</a></li>

				<?php endwhile;
				?>
			</ul>
		</div>
	</div>
</section>
<!-- End trending Articles -->
<section id="article-flow-2">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<ul class="articles">
					<div class="row">
						<?php $args6 = array('posts_per_page' => 1, 'offset' => 6);
						$query6 = new WP_Query( $args6 );
						if ( $query6->have_posts() ) { while ( $query6->have_posts() ) { $query6->the_post(); ?>
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
								$thumb_url = $thumb_url_array[0];
							?>
							<li class="col-md-6"><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
									<h2><?php the_title(); ?></h2>
								</div>
							</a></li>
						<?php } wp_reset_query(); wp_reset_postdata(); } ?>
						
						<li class="col-md-6">
							<ul class="secondary-list articles">
								<?php $args7 = array('posts_per_page' => 2, 'offset' => 7);
								$query7 = new WP_Query( $args7 );
								if ( $query7->have_posts() ) { while ( $query7->have_posts() ) { $query7->the_post(); ?>
									<?php
										$thumb_id = get_post_thumbnail_id();
										$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
										$thumb_url = $thumb_url_array[0];
									?>
									<div class="row">
										<li><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
												<h2><?php the_title(); ?></h2>
											</div>
										</a></li>
									</div>
								<?php } wp_reset_query(); wp_reset_postdata(); } ?>
							</ul>
						</li>
					</div>
					<div class="row">
						<?php $args7 = array('posts_per_page' => 3, 'offset' => 9);
						$query7 = new WP_Query( $args7 );
						if ( $query7->have_posts() ) { while ( $query7->have_posts() ) { $query7->the_post(); ?>
							<?php
								$thumb_id = get_post_thumbnail_id();
								$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
								$thumb_url = $thumb_url_array[0];
							?>
							<li class="col-md-4"><a href="<?php the_permalink(); ?>" style="background-image: url('<?php echo $thumb_url; ?>')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></span></p>
									<h2><?php the_title(); ?></h2>
								</div>
							</a></li>
						<?php } wp_reset_query(); wp_reset_postdata(); } ?>
					</div>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- End article flow 2 -->
<section class="ad">
	<div class="container">
		<div class="row">
			<div class="col-md-8 ad-wrap text-center col-md-offset-2">
				<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
				<!-- AD2 -->
				<ins class="adsbygoogle"
     			style="display:inline-block;width:728px;height:90px"
     			data-ad-client="ca-pub-7118737131388553"
     			data-ad-slot="1478566822"></ins>
				<script>
				(adsbygoogle = window.adsbygoogle || []).push({});
				</script>
			</div>
		</div>
	</div>
</section>
<!-- End Ad -->
<section id="subscribe">
	<div class="container">
		<div class="row">
			<div class="col-md-8 subscription-form">
				<h3>Subscribe to our newsletter</h3>
				<p>Receive your free supplement starter guide</p>
				<div id="result"></div>
				<form class="email-submit">
					<input type="email" placeholder="Enter e-mail address" name="email" required>
					<input type="hidden" name="action" value="add_user_to_active_campaign">
					<button><i class="btm bt-envelope"></i></button>
				</form>
			</div>
		</div>
	</div>
</section>
<!-- End Subscribe -->
<?php get_footer(); ?>