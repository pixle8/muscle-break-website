<?php get_header(); 
// Template Name: BMI Calculator
?>
	<section id="bmr-banner" class="banner">
		<div class="container">
			<h2 class="page-title"><strong>BMI Calculator</strong></h2>
		</div>
	</section>
	<!-- End About Banner -->
	<section class="calculator-content" id="bmr-calc">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="text">
						<h4><strong>Your Body Mass Index (BMI)</strong> is a measure of body fat based on height and weight that applies to adult men and women. This will give you a very rough estimation of your relative health.</h4>
					</div>
				</div>
				<div class="col-md-7">
					<div class="calculator full-left" style="">
						<div class="units full-left">
							<div class="half pull-left">
								<p>Choose System:</p>
							</div>
							<div class="half pull-right">
								<div class="unit-selection">
									<div class="imperial current">
										<input type="radio" name="system" value="imperial" checked="checked">
										Imperial
									</div>
									<div class="metric">
										<input type="radio" name="system" value="metric">
										Metric
									</div>
								</div>
							</div>
							<div class="clear-fix"></div>
						</div>
						<form class="full-left">
							<div class="age-height"> <!-- Age/Height -->
								<div class="half pull-left">
									<div class="weight-wrap imperial">
										<label for="age">
											Weight:
										</label>
										<div class="input-wrap">
											<input type="number" name="weightlbs" value="150" placeholder="150">
											<span>Lbs</span>
										</div>
									</div>

									<div class="weight-wrap metric hidden">
										<label for="age">
											Weight:
										</label>
										<div class="input-wrap">
											<input type="number" name="weightkg" value="80" placeholder="80">
											<span>Kg</span>
										</div>
									</div>
								</div>
								<div class="half pull-right">
									<div class="height-wrap imperial">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="number" name="heightft" value="5" placeholder="5">
											<span>ft</span>
											<input type="number" name="heightin" value="10" placeholder="10">
											<span>in</span>
										</div>
									</div>
									<div class="height-wrap metric hidden">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="number" name="heightcm" placeholder="175" value="175">
											<span>cm's</span>
										</div>
									</div>
								</div>
							</div>							
						</form>

						<div id="results" style="display: none;">
							<p>Your BMI Is: <span class="cal-result-val"></span></p>
						</div>
						<div class="submit full-left"> <!-- Submit -->
							<div class="calculate-btn">
								<div>Calculate My BMI</div>
							</div>
						</div>
						<!-- End Submit -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<script src="<?php bloginfo('template_directory');?>/js/bmi.js"></script>

	<div id="lightbox-opt" class="hidden">
		<h3>Where should we send your results?</h3>
		<p>Please enter your email below and we will send your results</p>
        <span id="result-top"></span>
		<form class="email-submit">
			<input type="email" name="email" placeholder="Your Email">
			<input type="submit" value="Send Me My Results!" class="bmi-calc-email-submit">
            <input type="hidden" name="action" value="add_user_to_active_campaign">
            <input type="hidden" name="bmi" value="0" id="calc_value">
		</form>
	</div>
	<div id="lightbox-cover" class="hidden"></div>
	<!-- end lightbox opt -->
<?php get_footer(); ?>