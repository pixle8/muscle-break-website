<?php get_header(); ?>
<section id="author-banner">
	<div class="container">
		<div class="row">
			<div class="col-sm-4">
				<?php
					$the_user_id = get_the_author_meta('ID');
					$image = get_field('avatar', 'user_' . $the_user_id);
				?>
				<div class="image" style="background-image: url(<?php echo $image; ?>);">
					
				</div>

				<ul class="share-links">
					<?php 
					$user_id = get_the_author_meta('ID');
					$post_id = 'user_'.$user_id;

					$facebookurl = get_field('facebook_url', $post_id);
					$twitterurl = get_field('twitter_url', $post_id);
					$instagramurl = get_field('instagram_url', $post_id);
					$linkedinurl = get_field('linkedin_url', $post_id);

					if( $facebookurl ) {
					    echo '<li><a href="' . $facebookurl . '"><i class="fab fab-facebook-alt"></i></a></li>';
					}
					if( $twitterurl ) {
					    echo '<li><a href="' . $twitterurl . '"><i class="fab fab-twitter"></i></a></li>';
					}
					if( $instagramurl ) {
					    echo '<li><a href="' . $instagramurl . '"><i class="fab fab-instagram"></i></a></li>';
					}
					if( $linkedinurl ) {
					    echo '<li><a href="' . $linkedinurl . '"><i class="fab fab-linkedin"></i></a></li>';
					}
					 ?>
				</ul>
			</div>
			<div class="col-sm-8">
				<div class="content">
					<?php the_post(); ?>
					
					<h2><?php the_author_link(); ?></h2>
					<h4>Background</h4>
					<p><?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', '<div class="archive-meta">' . get_the_author_meta( 'user_description' ) . '</div>' ); ?></p>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Author Banner -->

<section id="content-articles">
	<div class="container">
		<div class="title">
			<h3>latest articles</h3>
		</div>

		<ul class="articles-list">
			<?php rewind_posts(); ?>
			<?php
			$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
			$authorID = $author->ID;
			$args = array(
			    'author' => $authorID,
			    'posts_per_page' => 6,
			    'paged' => 1,
			    'post_type' => array('post'),
			    'post_status' => 'publish',
			    'orderby' => 'post_date',
			    'order' => 'DESC',
			    'ignore_sticky_posts' => 1,
			);

			$loop = new WP_Query($args);
			while ($loop->have_posts()) : $loop->the_post(); ?>
				<?php 
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
					$thumb_url = $thumb_url_array[0];
				?>
				<li>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="image"><img src="<?php echo $thumb_url; ?>" alt=""></a>
						<h5><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h5>
					</div>
				</li>
			<?php endwhile;?>
		</ul>
		<div class="row text-center" id="more-posts">
			<input id="max-pages" type="hidden" value="<?php echo $loop->max_num_pages; ?>">
			<a data-page="2" data-author="<?php echo $authorID; ?>" data-page-type="author" href="#"><i class="btm bt-sync"></i> Load More Posts</a>
		</div>
	</div>
</section>
<!-- Content Articles -->
<?php get_footer(); ?>