<?php get_header(); ?>
<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
<?php // if ( ! post_password_required() ) comments_template( '', true ); ?>
<?php
	$thumb_id = get_post_thumbnail_id();
	$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
	$thumb_url = $thumb_url_array[0];
	wpb_set_post_views(get_the_ID());

?>
<section id="article-banner" style="background-image: url('<?php echo $thumb_url; ?>');">
	<div class="overlay"></div>
	<div class="container">
		<div class="row">
			<div class="col-md-9 col-md-offset-2 text-center">
				<div class="text-wrap">
					<p class="cat"><?php $categories = get_the_category(); if ( ! empty( $categories ) ) {echo esc_html( $categories[0]->name );} ?></p>
					<h1><?php the_title(); ?></h1>
				</div>
				<div class="social-block">
					<div class="inner">
						<ul>
							<li>
								<p><span><i class="btb bt-share"></i></span></p>
							</li>
							<li>
								<a target="_blank" href="javascript:myPopup('https://www.facebook.com/sharer/sharer.php?u=<?php echo get_permalink(); ?>')"><i class="fab fab-facebook-alt"></i></a>
							</li>
							<li>
								<a target="_blank" href="javascript:myPopup('https://twitter.com/home?status=<?php echo get_permalink(); ?>')"><i class="fab fab-twitter"></i></a>
							</li>
							<li>
								<a target="_blank" href="javascript:myPopup('https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>&media=<?php echo $thumb_url; ?>&description=<?php echo get_the_excerpt(); ?>')" class="link-pinterest"><i class="fab fab-pinterest-alt"></i></a>
							</li>
							<li>
								<a href="mailto:?to=&body=Check out this article from MuscleBreak.com!%0D%0A%0D%0A<?php echo get_the_excerpt(); ?>%0D%0A%0D%0A<?php echo get_permalink(); ?>&subject=<?php the_title(); ?>" class="link-envelope"><i class="btm bt-envelope"></i></a>
							</li>
						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<!-- End Article Banner -->
<section id="content">
	<div class="container">
		<div class="row">
			<div class="col-md-8">
				<div class="post-text">
					<span class="post-content"><?php the_content(); ?></span>
					<?php
					 	$defaults = array(
							'before'           => '<div class="pagination">' . __( '' ),
							'after'            => '</div>',
							'link_before'      => '',
							'link_after'       => '',
							'next_or_number'   => 'next',
							'separator'        => '<span class="pageOf">  ' . $page . ' of ' . $numpages . '  </span>',
							'nextpagelink'     => __( 'Next <i class="fa fa-angle-double-right" aria-hidden="true"></i>' ),
							'previouspagelink' => __( '<i class="fa fa-angle-double-left" aria-hidden="true"></i> Prev' ),
							'pagelink'         => '%',
							'echo'             => 1
						);
					 		
					 		if($page == 1 && $page < $numpages) {
					 			echo '<span class="pageOf">  ' . $page . ' of ' . $numpages . '  </span>';
					 		}
					        wp_link_pages( $defaults );
					 		if($page > 1 && $page == $numpages) {
					 			echo '<span class="pageOf">  ' . $page . ' of ' . $numpages . '  </span>';
					 		}
					?>
				</div>
				<ul class="share-buttons">
					<li><a class="link-facebook" target="_blank" href="javascript:myPopup('http://www.facebook.com/share.php?<?php echo get_permalink(); ?>')"><i class="fab fab-facebook-alt"></i></a></li>
					<li><a target="_blank" href="javascript:myPopup('https://twitter.com/home?status=<?php echo get_permalink(); ?>')" class="link-twitter"><i class="fab fab-twitter"></i></a></li>
					<li><a target="_blank" href="javascript:myPopup('https://pinterest.com/pin/create/button/?url=<?php echo get_permalink(); ?>&media=<?php echo $thumb_url; ?>&description=<?php echo get_the_excerpt(); ?>')" class="link-pinterest"><i class="fab fab-pinterest-alt"></i></a></li>
					<li><a href="mailto:?to=&body=Check out this article from MuscleBreak.com!%0D%0A%0D%0A<?php echo get_the_excerpt(); ?>%0D%0A%0D%0A<?php echo get_permalink(); ?>&subject=<?php the_title(); ?>" class="link-envelope"><i class="btm bt-envelope"></i></a></li>
				</ul>
				<div class="subscribe">
					<div class="subscribe-text">
						<h3>Subscribe to our newsletter</h3>
						<p>GET YOUR FREE SUPPLEMENT STARTER GUIDE</p>
					</div>

					<div class="form-wrap">
						<form class="email-submit">
							<input type="email" class="field" placeholder="Enter your e-mail address" name="email" required>
							<input type="hidden" name="action" value="add_user_to_active_campaign">
							<button class="submit"><i class="btm bt-envelope"></i></button>
						</form>
						<div id="result"></div>
					</div>
				</div>
				<div class="author">
					<div class="image-wrap">
						<?php 
							$user_email = get_the_author_meta( 'user_email' ); 
							$user_gravatar_url = 'http://www.gravatar.com/avatar/' . md5($user_email) . '?s=80';
							$user_id = get_the_author_meta(ID);
							$nice_name = get_the_author_meta(nicename);
							// echo $nice_name;

							$site_url = esc_url( home_url( '/' ) );

							if($nice_name == 'muscle-break') {
								// do nothing
							} else {
								$profile_link = $site_url . 'author/' . $nice_name;	
								// echo "else";
							}
							
							// echo $nice_name;
							$image = get_field('avatar', 'user_' . $user_id);
						?>
						<a href="<?php echo $profile_link; ?>" class="author-link"><img src="<?php echo $image; ?>" alt=""></a>
						<a href="<?php echo get_site_url(); ?>/contact-us" class="link">contribute?</a>
					</div>
					<div class="text-wrap">
						<h4><a href="<?php echo $profile_link; ?>" class="author-link"><?php $name = get_the_author_meta(display_name); echo $name; ?></a></h4>
						<p><?php if ( '' != get_the_author_meta( 'user_description' ) ) echo apply_filters( 'archive_meta', get_the_author_meta( 'user_description' )); ?></p>
					</div>
				</div>

				<div id="ads">
					<?php
						$tags = get_the_tags();
						foreach ( $tags as $tag ) {
							$tag_link = get_tag_link( $tag->term_id );
									
							$html .= "<a href='{$tag_link}' title='{$tag->name} Tag' class='{$tag->slug} tag'>";
							$html .= "{$tag->name}</a>";
						}
						echo $html;
					?>

					<style type="text/css">
						.tag {display: inline-block;padding: 15px 10px;border: solid 1px rgba(0, 0, 0, 0.38);margin-right: 10px;margin-bottom: 10px;}
					</style>
				</div>

				<!-- google ad -->
				<div class="post-content-ad" align="center">
					<style>
					.responsive { width: 234px; height: 60px; }
					@media(min-width: 400px) { .responsive { width: 320px; height: 100px; } }
					@media(min-width: 500px) { .responsive { width: 468px; height: 60px; } }
					@media(min-width: 800px) { .responsive { width: 728px; height: 90px; } }
					</style>
					<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
					<!-- responsive -->
					<ins class="adsbygoogle responsive"
     				style="display:block"
     				data-ad-client="ca-pub-7118737131388553"
     				data-ad-slot="9253633220"
    				data-ad-format="horizontal"></ins>
					<script>
					(adsbygoogle = window.adsbygoogle || []).push({});
					</script>
				</div>
				<div class="related-posts">
					<h3>Related Posts</h3>
					<ul>
						<?php
						$related = get_posts( array( 'category__in' => wp_get_post_categories($post->ID), 'numberposts' => 3, 'post__not_in' => array($post->ID) ) );
						if( $related ) foreach( $related as $post ) {
						setup_postdata($post); 
						$thumb_id = get_post_thumbnail_id();
						$thumb_url_array = wp_get_attachment_image_src($thumb_id, '', true);
						$thumb_url = $thumb_url_array[0]; ?>
							<li>
								<a href="<?php the_permalink(); ?>"><div class="related-thumb" style="background-image: url(<?php echo $thumb_url; ?>);"></div></a>
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
							</li>
						<?php } wp_reset_postdata(); ?>
					</ul>
				</div>
				<div class="comments">
					<!-- <img src="<?php bloginfo('template_directory');?>/images/comments.jpg" alt=""> -->
					<div id="disqus_thread"></div>
					<script>
					(function() { // DON'T EDIT BELOW THIS LINE
					var d = document, s = d.createElement('script');
					s.src = '//musclebreak.disqus.com/embed.js';
					s.setAttribute('data-timestamp', +new Date());
					(d.head || d.body).appendChild(s);
					})();
					</script>
					<noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
				</div>
			</div>
			<?php endwhile; endif; ?>
			<div class="col-md-4">
				<?php get_sidebar(); ?>
			</div>
		</div>
	</div>
</section>
<!-- Content -->

<script>
(function($){
    // console.log(navigator.userAgent);
    /* Adjustments for Safari on Mac */
    if (navigator.userAgent.indexOf('Safari') != -1 && navigator.userAgent.indexOf('Chrome') == -1) {
        // console.log('Safari on Mac detected, applying class...');
        $('.pageOf').addClass('safari'); // provide a class for the safari-mac specific css to filter with
    }
})(jQuery);
</script>
<?php if($nice_name == 'muscle-break') { ?>
	<script type="text/javascript">
		$("a.author-link").removeAttr("href");								
	</script>
<?php } ?>
<?php get_footer(); ?>