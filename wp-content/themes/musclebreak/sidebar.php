<div class="side-bar">
	<div class="ad" align="center">
		<style>
		@media(min-width: 800px) { .sidebar-adunit { width: 300px; height: 250px; } }
		</style>
		<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"></script>
		<!-- sidebar-adunit -->
		<ins class="adsbygoogle sidebar-adunit"
     		style="display:block"
     		data-ad-client="ca-pub-7118737131388553"
     		data-ad-slot="5554156827"
     		data-ad-format="rectangular"></ins>
		<script>
		(adsbygoogle = window.adsbygoogle || []).push({});
		</script>
	</div>
	<div class="newsletter">
		<div class="upper-box text-center">
			<h3><span class="line1">download your free</span><br><span class="line2">supplement</span><br><span class="line3">starter Guide</span></h3>
		</div>
		<div class="lower-box">
			<div id="sidebarresult"></div>
			<form class="email-submit">
				<input type="email" class="email-input" placeholder="Enter e-mail address" name="email" required>
				<input type="hidden" name="action" value="add_user_to_active_campaign">
				<button class="submit"><i class="btm bt-envelope"></i></button>
			</form>
		</div>
	</div>
	<div class="featured-articles">
		<div class="menu-posts">
			<ul>
				<li class="active"><a href="#trending-posts">Trending</a></li>
				<li><a href="#recent-posts">Recent</a></li>
			</ul>
		</div>
		<div class="articles-wrap">
			<ul id="trending-posts">
			<?php 
			$popularpost = new WP_Query( array( 'posts_per_page' => 5, 'meta_key' => 'wpb_post_views_count', 'orderby' => 'meta_value_num', 'order' => 'DESC'  ) );
			while ( $popularpost->have_posts() ) : $popularpost->the_post(); 
			$thumb_id = get_post_thumbnail_id();
			$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'buzz-widget', true);
			$thumb_url = $thumb_url_array[0];
			$title = get_the_title();
			?>
			
			<li>
				<div class="article">
					<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb_url; ?>"></a>
					<p><a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth($title, 0, 35, '...'); ?></a></p>
				</div>
			</li>

			<?php endwhile;
			?>
			</ul>
			<ul id="recent-posts">
			<?php $args2 = array('posts_per_page' => 5, 'offset' => 0);
			$query2 = new WP_Query( $args2 );
			if ( $query2->have_posts() ) { while ( $query2->have_posts() ) { $query2->the_post(); ?>
				<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'buzz-widget', true);
					$thumb_url = $thumb_url_array[0];
					$title = get_the_title();
				?>
				<li>
					<div class="article">
						<a href="<?php the_permalink(); ?>"><img src="<?php echo $thumb_url; ?>"></a>
						<p><a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth($title, 0, 35, '...'); ?></a></p>
					</div>
				</li>
			<?php } wp_reset_query(); wp_reset_postdata(); } ?>
			</ul>
		</div>
	</div>
	<div class="facebook-widget">
		<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmusclebreak%2F&tabs&width=300&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="326px" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
	</div>
</div>