<?php get_header(); ?>
	<section id="content">
		<div class="container">
			<h2 class="page-title"><?php _e( 'Tag Archives: ', 'musclebreak' ); ?><?php single_tag_title(); ?></h2>
			<div class="row">
				<div class="col-md-8">
					<!--<div class="post-content-ad">
						<img src="<?php bloginfo('template_directory');?>/images/ad.jpg" alt="">
					</div>-->
					<div class="posts-list">
						<?php 
						$tag = get_queried_object(); 
						$tagSlug = $tag->slug;
						$args = array(
        					'tag' => $tagSlug,
						    'posts_per_page' => 6,
						    'paged' => 1,
						    'post_type' => array('post'),
						    'post_status' => 'publish',
						    'orderby' => 'post_date',
						    'order' => 'DESC',
						    'ignore_sticky_posts' => 1,
						);

						$loop = new WP_Query($args);
						 ?>

						<?php if ( $loop->have_posts() ) : while ( $loop->have_posts() ) : $loop->the_post(); ?>
						<?php //get_template_part( 'entry' ); ?>
						<?php 
							$thumb_id = get_post_thumbnail_id();
							$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
							$thumb_url = $thumb_url_array[0];
						?>
							<article class="post">
								<a href="<?php the_permalink(); ?>" class="image"><img src="<?php echo $thumb_url; ?>" alt=""></a>
								<h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
								<p><?php echo excerpt(14); ?>...</p>
							</article>
						<?php endwhile; endif; ?>
					</div>
					<div class="row text-center" id="more-posts">
						<input id="max-pages" type="hidden" value="<?php echo $loop->max_num_pages; ?>">
						<a data-page="2" data-tag="<?php echo $tagSlug; ?>" href="#"><i class="btm bt-sync"></i> Load More Posts</a>
					</div>
				</div>
				<div class="col-md-4">
					<?php get_sidebar(); ?>
				</div>
			</div>
		</div>
	</section>
	<!-- Content -->
<?php get_footer(); ?>