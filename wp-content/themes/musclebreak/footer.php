		<section id="red-navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-10 nav-wrap">
						<ul class="nav-menu">
							<?php wp_nav_menu( array( 'container' => false, 'theme_location' => 'footer-menu' ) ); ?>
						</ul>
					</div>
					<div class="col-md-2 social">
						<ul class="social-menu">
							<li><a href="https://www.facebook.com/musclebreak/" target="_blank"><i class="fab fab-facebook-alt"></i></a></li>
							<li><a href="https://twitter.com/musclebreakcom" target="_blank"><i class="fab fab-twitter"></i></a></li>
							<li><a href="https://www.youtube.com/channel/UCPPl4UIXIozuMGmN1hzPR_g" target="_blank"><i class="fab fab-youtube"></i></a></li>
							<li><a href="https://www.instagram.com/musclebreakcom/" target="_blank"><i class="fab fab-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-10 copyright">
						<p>&copy; <?php echo date("Y") ?> musclebreak. all rights reserved</p>
					</div>
					<div class="col-md-2 contact">
						<a href="#" id="back-to-top" class="button clear">Back To Top</a>
					</div>
				</div>
			</div>
		</section>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="<?php bloginfo('template_directory');?>/js/functions.js"></script>
		<script>
			var templateDirectory = "<?php bloginfo('template_directory'); ?>";
		</script>

		<script>(function(d, s, id) {
		  var js, fjs = d.getElementsByTagName(s)[0];
		  if (d.getElementById(id)) return;
		  js = d.createElement(s); js.id = id;
		  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.8";
		  fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));</script>

		<script>
	        jQuery('iframe').each(function(i,ele){
		        str = jQuery(this).attr('src');
				if (str.indexOf("www.youtube.com") >= 0) {
					jQuery(this).wrap('<div class="video-container"></div>');
				}
	        });
	    </script>
	<?php wp_footer(); ?>
	</body>
</html>