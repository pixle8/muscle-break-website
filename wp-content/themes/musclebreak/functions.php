<?php
// Enable featured image
add_theme_support('post-thumbnails');


define("THEME_DIR", get_template_directory());

// Include options
if (!class_exists('ReduxFramework') && file_exists(dirname(__FILE__) . '/SiteOptions/ReduxCore/framework.php')) {
    require_once(dirname(__FILE__) . '/SiteOptions/ReduxCore/framework.php');
}
if (!isset($redux_demo) && file_exists(dirname(__FILE__) . '/SiteOptions/options/config.php')) {
    require_once(dirname(__FILE__) . '/SiteOptions/options/config.php');
}

// Enable LiveMode
function liveMode()
{
    if (class_exists('ReduxFrameworkPlugin')) {
        remove_filter('plugin_row_meta', array(ReduxFrameworkPlugin::get_instance(), 'plugin_metalinks'), null, 2);
    }
    if (class_exists('ReduxFrameworkPlugin')) {
        remove_action('admin_notices', array(ReduxFrameworkPlugin::get_instance(), 'admin_notices'));
    }
}

add_action('init', 'liveMode');


/*
* Custom Image Sizes
*/
add_action('after_setup_theme', 'iifym_theme_setup');
function iifym_theme_setup()
{
    add_image_size('buzz-widget', 90, 88, array('center', 'center')); // 300 pixels wide (and unlimited height)
    add_image_size('post-roll-thumb', 353, 235, true);
}

// Advance Custom Fields
add_filter('acf/settings/path', 'my_acf_settings_path');

function my_acf_settings_path($path)
{
    $path = get_stylesheet_directory() . '/acf/';
    return $path;
}

add_filter('acf/settings/dir', 'my_acf_settings_dir');

function my_acf_settings_dir($dir)
{
    $dir = get_stylesheet_directory_uri() . '/acf/';
    return $dir;
}

add_filter('acf/settings/show_admin', '__return_true');
include_once(get_stylesheet_directory() . '/acf/acf.php');

// Register Menus
add_action('init', 'register_my_menus');
function register_my_menus()
{
    register_nav_menus(array('main-menu' => __('Main Menu')));
    register_nav_menus(array('footer-menu' => __('Footer Menu')));
}

// Remove ULs
function remove_ul($menu)
{
    return preg_replace(array('#^<ul[^>]*>#', '#</ul>$#'), '', $menu);
}

add_filter('wp_nav_menu', 'remove_ul');

// CPTs
function all_custom_post_types()
{
    $types = array(
        array(
            'the_type' => 'links',
            'single' => 'Link',
            'plural' => 'Links',
            'menu_icon' => 'dashicons-businessman',
            'menu_name' => 'Links'
        )
    );

    foreach ($types as $type) {
        // Set the post variables from the array
        $the_type = $type['the_type'];
        $single = $type['single'];
        $plural = $type['plural'];
        $menu_icon = $type['menu_icon'];
        $menu_name = $type['menu_name'];
        // Create some new post variables
        $taxname = $the_type . '-category';
        $tagname = $the_type . '-tag';
        // Post labels
        $labels = array(
            'name' => $plural,
            'singular_name' => $single,
            'menu_name' => $menu_name
        );
        // Arguments for the Post Type
        $args = array(
            'labels' => $labels,
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_menu' => true,
            'query_var' => true,
            'rewrite' => array('slug' => $the_type, 'with_front' => true),
            'capability_type' => 'post',
            'has_archive' => true,
            'hierarchical' => false, // Disable Drag and Drop Sort Order
            'menu_icon' => $menu_icon,
            'taxonomies' => array($taxname, $tagname),
            'show_in_nav_menus' => false,
            'supports' => array('title', 'editor', 'thumbnail', 'comments')
        );
        // Register the Post Type
        register_post_type($the_type, $args);
        // Custom Taxonomy labels
        $labels = array(
            'name' => $plural . ' Categories',
            'singular_name' => $single . ' Category',
            'menu_name' => 'Categories'
        );

        // Register the Taxonomy
        register_taxonomy($taxname, $the_type, array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'has_archive' => true,
            'show_in_nav_menus' => true,
            'rewrite' => array('slug' => $taxname),
        ));
        // Custom Tags labels
        $labels = array(
            'name' => $plural . ' Tags',
            'singular_name' => $single . ' Tag',
            'menu_name' => 'Tags'
        );
        // Register the Tags
        register_taxonomy($tagname, $the_type, array(
            'hierarchical' => false,
            'labels' => $labels,
            'show_ui' => true,
            'query_var' => true,
            'has_archive' => true,
            'show_in_nav_menus' => false,
            'rewrite' => array('slug' => $tagname),
        ));
    }
}

add_action('init', 'all_custom_post_types');


/*
 * Sidebar
 */

/**
 * Register our sidebars and widgetized areas.
 *
 */
function sidebars_widgets_init()
{

    register_sidebar(array(
        'name' => 'Sidebar',
        'id' => 'sidebar',
        'before_widget' => '<li class="widget ">',
        'after_widget' => '</li>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    register_sidebar(array(
        'name' => 'Header Widget Area',
        'id' => 'header-widget',
        'before_widget' => '<div class="widget ">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));

    register_sidebar(array(
        'name' => 'Links Page Widget Area',
        'id' => 'links-page-widget',
        'before_widget' => '<div class="widget ">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));


}

add_action('widgets_init', 'sidebars_widgets_init');


// Load More Function
if (file_exists(THEME_DIR . '/incl/custom_functions/load_more.php')) {
    require_once(THEME_DIR . '/incl/custom_functions/load_more.php');
}

//Add ajaxulr to front-end
add_action('wp_head', 'musclebreak_ajaxurl');
if (!function_exists('musclebreak_ajaxurl')) {
    function musclebreak_ajaxurl()
    {
        ?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        </script>
        <?php
    }
}


function getPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count . ' Views';
}

function setPostViews($postID)
{
    $count_key = 'post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if ($count == '') {
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    } else {
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}

function getMostPopularPosts()
{
    $args = array(
        'post_type' => 'post',
        'orderby' => 'meta_value_num',
        'meta_key' => 'post_views_count',
        'posts_per_page' => 3,
        'ignore_sticky_posts' => 1,
    );
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()): $query->the_post(); ?>
            <div class="post">
                <a href="<?php the_permalink(); ?>">
                    <div class="post-image-holder"><?php the_post_thumbnail('related-thumb'); ?></div>
                    <div class="post-content-holder"><p><?php the_title(); ?></p></div>
                </a>
            </div>
            <?php
        endwhile;
        wp_reset_postdata();
        wp_reset_query();
    endif;
}


function getFeaturedPopularPosts()
{

    $args = array(
        'posts_per_page' => 3,
        'post_type' => 'post',
        'meta_key' => 'featured_post',
        'meta_value' => '1',
        'ignore_sticky_posts' => 1,
    );
    $query = new WP_Query($args);
    if ($query->have_posts()):
        while ($query->have_posts()): $query->the_post();

            ?>
            <div class="post">
                <a href="<?php the_permalink(); ?>">
                    <div class="post-image-holder"><?php the_post_thumbnail('related-thumb'); ?></div>
                    <div class="post-content-holder"><p><?php the_title(); ?></p></div>
                </a>
            </div>

        <?php endwhile;
    endif;

    wp_reset_query();
}

function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  } 
  $excerpt = preg_replace('`\[[^\]]*\]`','',$excerpt);
  return $excerpt;
}

function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  } 
  $content = preg_replace('/\[.+\]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]&gt;', $content);
  return $content;
}

// Post tracking function
function wpb_set_post_views($postID) {
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        $count = 0;
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
    }else{
        $count++;
        update_post_meta($postID, $count_key, $count);
    }
}
//To keep the count accurate, lets get rid of prefetching
remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);

function wpb_track_post_views ($post_id) {
    if ( !is_single() ) return;
    if ( empty ( $post_id) ) {
        global $post;
        $post_id = $post->ID;    
    }
    wpb_set_post_views($post_id);
}
add_action( 'wp_head', 'wpb_track_post_views');

function wpb_get_post_views($postID){
    $count_key = 'wpb_post_views_count';
    $count = get_post_meta($postID, $count_key, true);
    if($count==''){
        delete_post_meta($postID, $count_key);
        add_post_meta($postID, $count_key, '0');
        return "0 View";
    }
    return $count.' Views';
}


function bmi_email_content($bmi){
    $html = '<h1>Your BMI Results</h1>
            <h4>Your bmi is %s</h4>
            <p>Thanks for using the MuscleBreak BMI Calculator.';

    return sprintf($html,$bmi);
}

function bmr_email_content($bmi){

    $html = '<h1>Your BMR Results</h1>
            <h4>Your bmr is %s</h4>
            <p>Thanks for using the MuscleBreak BMR Calculator.';


    return sprintf($html,$bmi);

}


function send_email_to_user($to,$message,$subject){

    $headers = array('Content-Type: text/html; charset=UTF-8');

    return wp_mail($to,$subject,$message,$headers);
}


/*
 * Add user to active campaign
 */
function add_user_to_active_campaign_func(){
    $response=array();
    if(!array_key_exists('email',$_POST) || empty($_POST['email'])){
       //header('Status: 500', TRUE, 500);
        //http_response_code(400);
        status_header(422);
        die(json_encode(array(
            'errors' => array(
                'status' => 422,
                'title' => 'Email field is required!',
                'detail' => 'Email field is required!',
            )
        )));

    }
    require_once __DIR__.'/incl/active-campaign-function.php';

    try{
        $result = add_user_to_active_campaign($_POST['email'],2);

        switch (true){
            case array_key_exists('bmi',$_POST):{
                if(send_email_to_user($_POST['email'],bmi_email_content($_POST['bmi']),'Your BMI Results')){
                    wp_die();
                }

                break;
            }
            case array_key_exists('bmr',$_POST):{
                if(send_email_to_user($_POST['email'],bmr_email_content($_POST['bmr']),'Your BMR Results')){
                    wp_die();
                }
                break;
            }
            case array_key_exists('rm',$_POST):{

                break;
            }
        }


    }catch (NonValidKeys $e){

        if($e->getCode() === 1){
            header('Status: 500', TRUE, 500);
            die(json_encode(array(
                'errors' => array(
                    'status' => 500,
                    'title' => 'We have encountered an internal problem, please try again.',
                    'detail' => 'We have encountered an internal problem, please try again.',
                )
            )));
        }
        if($e->getCode() === 2 ){
            header('Status: 500', TRUE, 500);
            die(json_encode(array(
                'errors' => array(
                    'status' => 500,
                    'title' => 'We have encountered an internal problem, please try again.',
                    'detail' => 'We have encountered an internal problem, please try again.',
                )
            )));
        }
        if($e->getCode() === 3 ){
            header('Status: 500', TRUE, 500);
            die(json_encode(array(
                'errors' => array(
                    'status' => 500,
                    'title' => 'Invalid email address provided!',
                    'detail' => 'Invalid email address provided!',
                )
            )));
        }
    }
}


add_action('wp_ajax_add_user_to_active_campaign', 'add_user_to_active_campaign_func');
add_action('wp_ajax_nopriv_add_user_to_active_campaign', 'add_user_to_active_campaign_func');

//Add ajaxulr to front-end
add_action('wp_head', 'add_ajax_url');
if (!function_exists('add_ajax_url')) {
    function add_ajax_url()
    {
        ?>
        <script type="text/javascript">
            var ajaxurl = '<?php echo admin_url('admin-ajax.php'); ?>';
        </script>
        <?php
    }
}

// Load More Function
if (file_exists(THEME_DIR . '/incl/custom_functions/load_more.php')) {
    require_once(THEME_DIR . '/incl/custom_functions/load_more.php');
}


// Remove emoji scripts
remove_action( 'wp_head', 'print_emoji_detection_script', 7 ); 
remove_action( 'admin_print_scripts', 'print_emoji_detection_script' ); 
remove_action( 'wp_print_styles', 'print_emoji_styles' ); 
remove_action( 'admin_print_styles', 'print_emoji_styles' );