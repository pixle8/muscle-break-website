<?php
if(!array_key_exists('email',$_POST)){
    header('Location: ' . $_SERVER['HTTP_REFERER']);
}

require_once __DIR__.'/incl/active-campaign-function.php';

try{
    $result = add_user_to_active_campaign($_POST['email'],2);
}catch (NonValidKeys $e){

    if($e->getCode() === 1){
        header('Location: ' . $_SERVER['HTTP_REFERER'].'?error=1');
    }
    if($e->getCode() === 2 ){
        header('Location: ' . $_SERVER['HTTP_REFERER'].'?error=2');
    }
    if($e->getCode() === 3 ){
        header('Location: ' . $_SERVER['HTTP_REFERER'].'?error=3');
    }
}


