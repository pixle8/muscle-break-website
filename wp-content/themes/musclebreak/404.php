<?php get_header(); ?>
<section id="banner-error">
	<div class="container">
		<h2 class="page-title">404</h2>
		<p>trust us, we looked everywhere</p>
	</div>
</section>
<!-- Banner Error -->

<section id="content-articles">
	<div class="container">
		<div class="title">
			<h4><em>instead</em> check out our</h4>
			<h3>latest articles</h3>
		</div>

		<ul class="articles-list">
			<?php $args = array('posts_per_page' => 6, 'offset' => 0);
			$query = new WP_Query( $args );
			if ( $query->have_posts() ) { while ( $query->have_posts() ) { $query->the_post(); ?>
				<?php
					$thumb_id = get_post_thumbnail_id();
					$thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
					$thumb_url = $thumb_url_array[0];
					$title = get_the_title();
				?>
				<li>
					<div class="item">
						<a href="<?php the_permalink(); ?>" class="image"><img src="<?php echo $thumb_url; ?>" alt=""><!--<i class="btm bt-share"></i>--></a>
						<h5><a href="<?php the_permalink(); ?>"><?php echo mb_strimwidth($title, 0, 35, '...'); ?></a></h5>
					</div>
				</li>
			<?php } wp_reset_query(); wp_reset_postdata(); } ?>
		</ul>
	</div>
</section>
<!-- Content Articles -->
<?php get_footer(); ?>