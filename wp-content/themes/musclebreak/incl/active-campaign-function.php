<?php
require_once __DIR__ . '/src-activecampaign/ActiveCampaign.class.php';

class NonValidKeys extends Exception {


    // Redefine the exception so message isn't optional
    public function __construct($message, $code = 0, Exception $previous = null) {
        // some code

        // make sure everything is assigned properly
        parent::__construct($message, $code, $previous);
    }


    // custom string representation of object
    public function __toString() {
        return __CLASS__ . ": [{$this->code}]: {$this->message}\n";
    }


}


function add_user_to_active_campaign($email,$id)
{

    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        throw new NonValidKeys('Invalid email address provided',3);
    }

    $list_id = $id; /// [IIFYM] Master List --  LIST ID.

    $ac = new ActiveCampaign('https://musclebreak.api-us1.com', '9c759dfa2cefe33c3a171188b8fb0418ce5a3397d38512a392b3e4fa1d5b466ab862fba2');
    if (!(int)$ac->credentials_test()) {
        throw new NonValidKeys('Invalid api keys provided',1);
        //return false;
    }
    /*
     * ADD OR EDIT CONTACT (TO THE SPECIFIC LIST).
    */
    $contact = array(
        "email" => $email,
        "first_name" => " ",
        "last_name" => "",
        "p[{$list_id}]" => $list_id,
        "status[{$list_id}]" => 1, // "Active" status
    );
    $contact_sync = $ac->api("contact/sync", $contact);

    if (!(int)$contact_sync->success) {// request failed
        throw new NonValidKeys('Request Failed',2);
        //return false;
    }

    return 'success';
}




