<?php
    /*
     * Load More Function
     */
function loadMore() {
    $page= $_GET['page'];
    $category = $_GET['category'];
    $tag = $_GET['tag'];
    $author = $_GET['author'];
    $pageType = $_GET['pageType'];
    $post_per_page = 6;

    // setup your query to get what you want
    $args = array(
        'tag' => $tag,
        'cat' => $category,
        'author' => $author,
        'posts_per_page' => $post_per_page,
        'paged' => $page,
        'post_type' => array('post'),
        'post_status' => 'publish',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'ignore_sticky_posts' => 1,
    );

    $loop = new WP_Query($args);

    if ($pageType == 'author') {
        while ($loop->have_posts()) : $loop->the_post();
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
            $thumb_url = $thumb_url_array[0];
            // // define the structure of your posts markup
            $output.='<li>';
                $output.='<div class="item">';
                    $output.='<a href="'.get_the_permalink().'" class="image"><img src="'.$thumb_url.'" alt=""></a>';
                    $output.='<h5><a href="'.get_the_permalink().'">'.get_the_title().'</a></h5>';
                $output.='</div>';
            $output.='</li>';
        endwhile;
    }
    else {
        while ($loop->have_posts()) : $loop->the_post();
            $thumb_id = get_post_thumbnail_id();
            $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'post-roll-thumb', true);
            $thumb_url = $thumb_url_array[0];
            $post_excerpt = get_the_excerpt();
            $post_excerpt = substr($post_excerpt, 0, 100);
            // // define the structure of your posts markup
            $output.='<article class="post">';
                $output.='<a href="'.get_the_permalink().'" class="image"><img src="'.$thumb_url.'" alt=""></a>';
                $output.='<h4><a href="'.get_the_permalink().'">'.get_the_title().'</a></h4>';
                $output.='<p>'.$post_excerpt.'...</p>';
            $output.='</article>';
        endwhile;
    }

    // the Loop
    echo $output;
    // Reset Query
    wp_reset_query();

    wp_die();

}

function test(){
    print_r('test');
    wp_die();
}


add_action('wp_ajax_loadMore', 'loadMore');
add_action('wp_ajax_nopriv_loadMore', 'loadMore'); // not really needed
