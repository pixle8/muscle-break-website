<?php include 'incl/header.php'; ?>
	<section id="article-banner">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<div class="text-wrap">
						<p class="date">September 22nd, 2016</p>
						<h1>Combating Shoulder Injury With rotator cuff exercises</h1>
					</div>
					<div class="social-block">
						<div class="inner">
							<ul>
								<li>
									<p><span><i class="btb bt-share"></i></span></p>
								</li>
								<li>
									<a href="#"><i class="fab fab-facebook-alt"></i></a>
								</li>
								<li>
									<a href="#"><i class="fab fab-twitter"></i></a>
								</li>
								<li>
									<a href="#"><i class="fab fab-instagram"></i></a>
								</li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="arrows">
			<div class="left">
				<i class="btm bt-angle-left"></i>
			</div>
			<div class="right">
				<i class="btm bt-angle-right"></i>
			</div>
		</div>
	</section>
	<!-- End Article Banner -->
	<section id="content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="post-text">
						<p>If you are serious about seeing muscle building results and are someone who is regularly hitting the gym working your shoulders, it’s time to also get serious about doing rotator cuff exercises.</p>
						<p>Many people brush these off, thinking their rotator cuffs are strong enough. After all, if they can shoulder press more than 50lbs in each arm, doesn’t it stand to reason that these muscles would be already quite strong?</p>
						<p>Not entirely.</p>
						<p>Some people do suffer from weak rotator cuff muscles and when they do, it can lead to disastrous problems as far as injuries are concerned.</p>
						<p>Your rotator cuff is going not be responsible for helping you execute every single shoulder movement you make, so it’s not a group of muscles to take lightly.</p>
						<p>Your rotator cuff is going not be responsible for helping you execute every single shoulder movement you make, so it’s not a group of muscles to take lightly.</p>
						<ul>
							<li><strong>The infraspinatus</strong> (responsible for lateral rotation of the arm and the initiation of arm abduction)</li>
							<li><strong>The infraspinatus</strong> (responsible for lateral rotation of the arm and the initiation of arm abduction)</li>
							<li><strong>The infraspinatus</strong> (responsible for lateral rotation of the arm and the initiation of arm abduction)</li>
							<li><strong>The infraspinatus</strong> (responsible for lateral rotation of the arm and the initiation of arm abduction)</li>
						</ul>
						<img src="images/shoulder.jpg" alt="">
					</div>
					<ul class="share-buttons">
						<li><a href="#" class="link-facebook"><i class="fab fab-facebook-alt"></i></a></li>
						<li><a href="#" class="link-twitter"><i class="fab fab-twitter"></i></a></li>
						<li><a href="#" class="link-pinterest"><i class="fab fab-pinterest-alt"></i></a></li>
						<li><a href="#" class="link-envelope"><i class="btm bt-envelope"></i></a></li>
					</ul>
					<div class="subscribe">
						<div class="subscribe-text">
							<h3>Subscribe to our newsletter</h3>
							<p>Receive exciting features, news & special offers</p>
						</div>

						<div class="form-wrap">
							<form action="">
								<input type="email" placeholder="Enter e-mail address" class="field">
								<button type="submit" class="submit"><i class="btm bt-envelope"></i></button>
							</form>
						</div>
					</div>
					<div class="author">
						<div class="image-wrap">
							<img src="images/shannon-clark.jpg" alt="">
							<a href="#" class="link">contribute?</a>
						</div>
						<div class="text-wrap">
							<h4>Shannon Clark</h4>
							<p>Shannon Clark holds a Degree in Exercise Science and Sports Performance and is a certified AFLCA personal trainer. She has been working in the health and fitness industry for the last 12 years and writes for bodybuilding.com, askmen.com and lifehack.org.</p>
						</div>
					</div>
					<div class="post-content-ad">
						<img src="images/ad2.jpg" alt="">
					</div>
					<div class="related-posts">
						<h3>Related Posts</h3>
						<ul>
							<li>
								<a href="#"><img src="images/related-1.jpg" alt=""></a>
								<h4><a href="#">Classic chickpea and turkey meatballs</a></h4>
							</li>
							<li>
								<a href="#"><img src="images/related-2.jpg" alt=""></a>
								<h4><a href="#">WBFF atlete meisha pijot talks with muscle break</a></h4>
							</li>
							<li>
								<a href="#"><img src="images/related-3.jpg" alt=""></a>
								<h4><a href="#">Fasted Cardio: fat loss booster or fad?</a></h4>
							</li>
						</ul>
					</div>
					<div class="previous-next">
						<div class="previous">
							<a href="#">
								<p><span>Previous Article</span></p>
								<p>STRONGLIF TS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</p>
							</a>
						</div>
						<div class="next text-right">
							<a href="#">
								<p><span>Next Article</span></p>
								<p>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</p>
							</a>
						</div>
					</div>
					<div class="comments">
						<img src="images/comments.jpg" alt="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="side-bar">
						<div class="ad">
							<img src="images/gudpod.jpg" alt="">
						</div>
						<div class="newsletter">
							<div class="upper-box">
								<h3>get your <strong>free supplementation Guide </strong></h3>
							</div>
							<div class="lower-box">
								<form action="#">
									<input type="email" class="email-input" placeholder="Enter e-mail">
									<button type="submit" class="submit"><i class="btm bt-envelope"></i></button>
								</form>
							</div>
						</div>
						<div class="featured-articles">
							<div class="menu-posts">
								<ul>
									<li class="active"><a href="#trending-posts">Trending</a></li>
									<li><a href="#recent-posts">Recent</a></li>
								</ul>
							</div>
							<div class="articles-wrap">
								<ul id="trending-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
								</ul>
								<ul id="recent-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="facebook-widget">
							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmusclebreak%2F&tabs&width=300&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Content -->
<?php include 'incl/footer.php'; ?>