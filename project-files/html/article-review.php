<?php include 'incl/header.php'; ?>
	<section id="article-banner" class="article-review">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="product-image">
						<img src="images/article-review-img.jpg" alt="">
					</div>
				</div>
				<div class="col-sm-8">
					<div class="banner-content">
						<a href="#" class="btn">Review</a>
						<h2>legion athletics: Full Body Shred review</h2>
						<div class="social-block">
							<div class="inner">
								<ul>
									<li>
										<p><span><i class="btb bt-share"></i></span></p>
									</li>
									<li>
										<a href="#"><i class="fab fab-facebook-alt"></i></a>
									</li>
									<li>
										<a href="#"><i class="fab fab-twitter"></i></a>
									</li>
									<li>
										<a href="#"><i class="fab fab-instagram"></i></a>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Article Banner -->
	<section id="content">
		<div class="container">
			<div class="row">
				<div class="col-md-8">
					<div class="post-text">
						<h4>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. </h4>
						<p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.</p>
						<p>Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
						<p>Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>
						<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>
					</div>
					<ul class="share-buttons">
						<li><a href="#" class="link-facebook"><i class="fab fab-facebook-alt"></i></a></li>
						<li><a href="#" class="link-twitter"><i class="fab fab-twitter"></i></a></li>
						<li><a href="#" class="link-pinterest"><i class="fab fab-pinterest-alt"></i></a></li>
						<li><a href="#" class="link-envelope"><i class="btm bt-envelope"></i></a></li>
					</ul>
					<div class="subscribe">
						<div class="subscribe-text">
							<h3>Subscribe to our newsletter</h3>
							<p>Download your free supplement starter guide</p>
						</div>

						<div class="form-wrap">
							<form action="">
								<input type="email" placeholder="Enter e-mail address" class="field">
								<button type="submit" class="submit"><i class="btm bt-envelope"></i></button>
							</form>
						</div>
					</div>
					<div class="author">
						<div class="image-wrap">
							<img src="images/shannon-clark.jpg" alt="">
							<a href="#" class="link">contribute?</a>
						</div>
						<div class="text-wrap">
							<h4>Shannon Clark</h4>
							<p>Shannon Clark holds a Degree in Exercise Science and Sports Performance and is a certified AFLCA personal trainer. She has been working in the health and fitness industry for the last 12 years and writes for bodybuilding.com, askmen.com and lifehack.org.</p>
						</div>
					</div>
					<div class="post-content-ad">
						<img src="images/ad2.jpg" alt="">
					</div>
					<div class="related-posts">
						<h3>Related Posts</h3>
						<ul>
							<li>
								<a href="#"><img src="images/related-1.jpg" alt=""></a>
								<h4><a href="#">Classic chickpea and turkey meatballs</a></h4>
							</li>
							<li>
								<a href="#"><img src="images/related-2.jpg" alt=""></a>
								<h4><a href="#">WBFF atlete meisha pijot talks with muscle break</a></h4>
							</li>
							<li>
								<a href="#"><img src="images/related-3.jpg" alt=""></a>
								<h4><a href="#">Fasted Cardio: fat loss booster or fad?</a></h4>
							</li>
						</ul>
					</div>
					<div class="previous-next">
						<div class="previous">
							<a href="#">
								<p><span>Previous Article</span></p>
								<p>STRONGLIF TS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</p>
							</a>
						</div>
						<div class="next text-right">
							<a href="#">
								<p><span>Next Article</span></p>
								<p>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</p>
							</a>
						</div>
					</div>
					<div class="comments">
						<img src="images/comments.jpg" alt="">
					</div>
				</div>
				<div class="col-md-4">
					<div class="side-bar">
						<div class="shares">
							<ul>
								<li>
									<a href="#" class="link-facebook"><i class="fab fab-facebook-alt"></i><span>17K</span></a>
								</li>
								<li>
									<a href="#" class="link-twitter"><i class="fab fab-twitter"></i><span>23K</span></a>
								</li>
								<li>
									<a href="#" class="link-instagram"><i class="fab fab-instagram"></i><span>5K</span></a>
								</li>
							</ul>
						</div>
						<div class="ad">
							<img src="images/gudpod.jpg" alt="">
						</div>
						<div class="newsletter shaped">
							<div class="upper-box">
								<h3>download your <strong>free</strong><span class="larger">supplement</span><span class="large">starter guide</span></h3>
							</div>
							<div class="lower-box">
								<form action="#">
									<input type="email" class="email-input" placeholder="Enter e-mail">
									<button type="submit" class="submit"><i class="btm bt-envelope"></i></button>
								</form>
							</div>
						</div>
						<div class="featured-articles">
							<div class="menu-posts">
								<ul>
									<li class="active"><a href="#trending-posts">Trending</a></li>
									<li><a href="#recent-posts">Recent</a></li>
								</ul>
							</div>
							<div class="articles-wrap">
								<ul id="trending-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
								</ul>
								<ul id="recent-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="facebook-widget">
							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmusclebreak%2F&tabs&width=300&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Content -->
<?php include 'incl/footer.php'; ?>