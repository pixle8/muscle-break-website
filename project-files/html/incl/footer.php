		<section id="red-navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-10 nav-wrap">
						<ul class="nav-menu">
							<li><a href="#">Resources</a></li>
							<li><a href="#">Advertise</a></li>
							<li><a href="#">Support</a></li>
							<li><a href="#">Disclaimer</a></li>
							<li><a href="#">RSS</a></li>
						</ul>
					</div>
					<div class="col-md-2 social">
						<ul class="social-menu">
							<li><a href="#"><i class="fab fab-facebook-alt"></i></a></li>
							<li><a href="#"><i class="fab fab-twitter"></i></a></li>
							<li><a href="#"><i class="fab fab-youtube"></i></a></li>
							<li><a href="#"><i class="fab fab-instagram"></i></a></li>
						</ul>
					</div>
				</div>
			</div>
		</section>
		<section id="footer">
			<div class="container">
				<div class="row">
					<div class="col-md-10 copyright">
						<p>Copyright <?php echo date("Y") ?> &copy; musclebreak. all rights reserved</p>
					</div>
					<div class="col-md-2 contact">
						<a href="#" class="button clear">contact us</a>
					</div>
				</div>
			</div>
		</section>
	</body>
	<footer>
		<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
		<script src="js/functions.js"></script>
	</footer>
</html>

