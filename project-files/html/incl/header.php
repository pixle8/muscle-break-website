<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<title>MuscleBreak</title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700|Roboto:400,700" rel="stylesheet">
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
		<link rel="stylesheet" href="css/styles.css">
		<link rel="stylesheet" href="css/black-tie.min.css">
		<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
	</head>
	
	<body>
		<section id="top-bar">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="logo-wrap">
							<a href="index.php"><img src="images/logo.png" alt=""></a>
						</div>
					</div>
					<div class="col-md-10 nav-wrap hide-mobile">
						<ul class="nav-menu">
							<li class="nav-item"><a href="#">Training</a></li>
							<li class="nav-item"><a href="#">Nutrition</a></li>
							<li class="nav-item"><a href="#">Recipes</a></li>
							<li class="nav-item"><a href="#">Interviews</a></li>
							<li class="nav-item"><a id="search-trigger" href="#"><i class="btb bt-search"></i></a>
								<div id="search-wrap" class="search hide-mobile hidden">
									<form class="searchform">
										<input type="text" placeholder="Search...">
										<button><i class="btb bt-search"></i></button>
									</form>
								</div>
							</li>
						</ul>
					</div>

					<div class="show-mobile col-md-2 float-right">
						<div class="button-wrap">
							<button class="hamburger">&#9776;</button>
							<button class="cross">&#735;</button>
						</div>
						<div class="menu">
						  <ul>
						    <a href="#"><li>Training</li></a>
						    <a href="#"><li>Nutrition</li></a>
						    <a href="#"><li>Recipes</li></a>
						    <a href="#"><li>Interviews</li></a>
						    <a href="#"><li><i class="btb bt-search"></i></li></a>
						  </ul>
						</div> 
					</div>
				</div>
			</div>
		</section>
		<!-- End top bar -->