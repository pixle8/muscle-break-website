<?php include 'incl/header.php'; ?>
	<section id="author-banner">
		<div class="container">
			<div class="row">
				<div class="col-sm-4">
					<div class="image">
						<img src="images/author-img.jpg" alt="">
					</div>

					<ul class="share-links">
						<li>
							<a href="#"><i class="fab fab-facebook-alt"></i></a>
						</li>
						<li>
							<a href="#"><i class="fab fab-twitter"></i></a>
						</li>
						<li>
							<a href="#"><i class="fab fab-instagram"></i></a>
						</li>
					</ul>
				</div>
				<div class="col-sm-8">
					<div class="content">
						<h2>Shannon Clark</h2>
						<h4>Background</h4>
						<p>Shannon Clark holds a degree in exercise science and sport performance from the University of Alberta in Edmonton. She has been working in the health and fitness field for over 10 years. She started her love for physical activity as a competitive figure skater at the tender age of 6. At age 16, she finished her skating career at the Western Canadian Championships. Upon hanging up her blades, she got hooked on fitness, and it's been that way ever since.</p>
						<p>Shannon went on to become a personal trainer and then made the decision to pursue education in the field, eventually finding her love of health and fitness writing. When she's not writing about fitness or working out herself, she loves training clients on the side, baking up the latest high-protein desserts (some of which turn out great, some which we won't speak of), and spending time with a good book or movie.</p>
						<p>Learning more about nutrition has always been a huge personal interest, and staying on top of the latest diet trends proves to be an ongoing experience. If you'd like to learn more about her, you can visit her website at <br><em><a href="http://" target="_blank">www.ShannonClarkFitness.com</a></em>.</p>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Author Banner -->
	
	<section id="content-articles">
		<div class="container">
			<div class="title">
				<h3>latest articles</h3>
			</div>

			<ul class="articles-list">
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img1.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">CARB BACKLOADING: WHAT THIS TECHNIQUE IS ALL ABOUT</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img2.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">TRANSFORMATION: HOW RAPPER RICK ROSS LOST 75 POUNDS</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img3.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">WBFF ATHLETE MEISHA PIJOT TALKS WITH MUSCLE BREAK</a></h5>
					</div>
				</li>
			</ul>

			<div class="post-content-ad">
				<img src="images/ad.jpg" alt="">
			</div>

			<ul class="articles-list">
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img4.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">CARB BACKLOADING: WHAT THIS TECHNIQUE IS ALL ABOUT</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img5.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">TRANSFORMATION: HOW RAPPER RICK ROSS LOST 75 POUNDS</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img6.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">WBFF ATHLETE MEISHA PIJOT TALKS WITH MUSCLE BREAK</a></h5>
					</div>
				</li>
			</ul>

			<div class="more-button text-center">
				<a href="#" class="btn">Load More</a>
			</div>
		</div>
	</section>
	<!-- Content Articles -->
<?php include 'incl/footer.php'; ?>