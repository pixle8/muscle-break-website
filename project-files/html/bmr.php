<?php include 'incl/header.php'; ?>
	<section id="bmr-banner" class="banner">
		<div class="container">
			<h2 class="page-title"><strong>BMR Calculator</strong></h2>
		</div>
	</section>
	<!-- End About Banner -->
	<section class="calculator-content" id="bmr-calc">
		<div class="container">
			<div class="row">
				<div class="col-md-5">
					<div class="text">
						<h4><strong>Basal metabolic rate (BMR)</strong> is the amount of energy expended while at rest in a neutrally temperate environment. Find out your BMR with this handy calculator!</h4>
						<p>
							Basal metabolic rate (BMR) is the amount of energy expended while at rest in a neutrally temperate environment, in the post-absorptive state (meaning that the digestive system is inactive, which requires about twelve hours of fasting).
						</p>
						<p>
							The release of energy in this state is sufficient only for the functioning of the vital organs, such as the heart, lungs, brain and the rest of the nervous system, liver, kidneys, sex organs, muscles and skin. BMR decreases with age and with the loss of lean body mass. Increasing muscle mass increases BMR.
						</p>
					</div>
				</div>
				<div class="col-md-7">
					<div class="calculator">
						<div class="units">
							<div class="half pull-left">
								<p>Choose System:</p>
							</div>
							<div class="half pull-right">
								<div class="unit-selection">
									<div class="imperial current">
										<input type="radio" name="system" value="imperial" checked>
										Imperial
									</div>
									<div class="metric">
										<input type="radio" name="system" value="metric" checked>
										Metric
									</div>
								</div>
							</div>
							<div class="clear-fix"></div>
						</div>
						<form>
							<!-- Gender -->
							<div class="gender">
								<label for="gender">Gender:</label>
								<div class="half pull-left">
									<div class="man select active">
										<div class="selection-circle"><div class="inner-selected"></div></div>
										Man
										<i class="btm bt-user-male"></i>
										<input type="radio" name="gender" value="female">
									</div>
								</div>
								<div class="half pull-right">
									<div class="woman select">
										<div class="selection-circle"><div class="inner-selected"></div></div>
										Woman
										<i class="btm bt-user-female"></i>
										<input type="radio" name="gender" value="female">
									</div>
								</div>
							</div>
							<!-- End Gender Row -->
							<div class="age-height"> <!-- Age/Height -->
								<div class="half pull-left">
									<div class="age-wrap">
										<label for="age">
											Age:
										</label>
										<div class="input-wrap">
											<input type="text" name="age" value="30">
											<span>years old</span>
										</div>
									</div>
								</div>
								<div class="half pull-right">
									<div class="height-wrap imperial">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="text" name="height[ft]" value="5">
											<span>ft</span>
											<input type="text" name="height[in]" value="10">
											<span>in</span>
										</div>
									</div>
									<div class="height-wrap metric">
										<label for="height">
											Height:
										</label>
										<div class="input-wrap">
											<input type="text" name="height[cm]" placeholder="200">
											<span>cm's</span>
										</div>
									</div>
								</div>
							</div>
							<!-- End Age/Height -->
							<div class="weight"> <!-- Weight -->
								<label for="weight">Weight:</label>
								<div class="slider weight-slider">
									<div id="weight-slider" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content"><span tabindex="0" class="ui-slider-handle ui-corner-all ui-state-default" style="left: 0%;"></span></div>
									<span class="min"><span class="imperial-min">0</span> <span class="metric-min">0</span></span>
									<span class="max"><span class="imperial-max">600</span> <span class="metric-max">227</span></span>
								</div>
								<input type="hidden" name="weight" id="weight-input">
							</div>
							<!-- End Weight -->
							<div class="activity"> <!-- Activity -->
								<label for="activity">Activity Level:</label>
								<div class="activity-wrap">
									<select name="activity" id="activity">
									  <option>No Activity</option>
									  <option>Light Activity</option>
									  <option selected="selected">Active</option>
									  <option>Very Active</option>
									</select>
								</div>
							</div>
							<!-- End Activity -->
						</form>
						<div class="submit"> <!-- Submit -->
							<div class="calculate-btn">
								<div>Calculate My BMR</div>
							</div>
						</div>
						<!-- End Submit -->
					</div>
				</div>
			</div>
		</div>
	</section>
<?php include 'incl/footer.php'; ?>