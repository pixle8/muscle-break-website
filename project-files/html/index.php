<?php include 'incl/header.php'; ?>
	<section id="home-banner">
		<div class="overlay"></div>
		<div class="container">
			<div class="row">
				<div class="col-md-8 col-md-offset-2 text-center">
					<h1>Carb Backloading: what this technique is all about</h1>
				</div>
				<div class="col-md-6 col-md-offset-3 text-center">
					<p>Wondering about carb backloading? You aren’t alone. This is one of the hottest trends in the nutrition world today for both those who are looking to build muscle as well as burn fat.</p>
				</div>
				<div class="col-md-4 col-md-offset-4 text-center">
					<a href="#" class="button red">Read More</a>
				</div>
			</div>
		</div>
	</section>
	<!-- End Home-banner -->
	<section id="article-flow-1">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="articles">
						<div class="row">
							<li class="col-md-3"><a href="#" style="background: url('images/articlebg1.jpg')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category">Training</span></p>
									<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
								</div>
							</a></li>
							<li class="col-md-6"><a href="#">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category">Training</span></p>
									<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
									<p class="excerpt">The Stronglifts 5X5 training program has become a well renowned programming method for anyone looking to develop size and strength.</p>
								</div>
							</a></li>
							<li class="col-md-3">
								<ul class="secondary-list">
									<div class="row">
										<li class="facebook">
											<div class="text-wrap">
												<h2>72,054</h2>
												<p>Fans</p>
												<a href="#" class="button">Like our page</a>
											</div>
										</li>
									</div>
									<div class="row">
										<li class="instagram">
											<div class="text-wrap">
												<h2>40,343</h2>
												<p>Followers</p>
												<a href="#" class="button">Follow Us</a>
											</div>
										</li>
									</div>
								</ul>
							</li>
						</div>
						<div class="row">
							<li class="col-md-6"><a href="#" style="background: url('images/homebg.jpg')">
								<div class="overlay"></div>
								<div class="text-wrap">
									<p><span class="category">Training</span></p>
									<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
									<p class="excerpt">The Stronglifts 5X5 training program has become a well renowned programming method for anyone looking to develop size and strength.</p>
								</div>
							</a></li>
							<li class="col-md-6">
								<ul class="secondary-list articles">
									<div class="row">
										<li><a href="#" style="background: url('images/articlebg2.jpg')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category">Training</span></p>
												<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
											</div>
										</a></li>
									</div>
									<div class="row">
										<li><a href="#"  style="background: url('images/articlebg3.jpg')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category">Training</span></p>
												<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
											</div>
										</a></li>
									</div>
								</ul>
							</li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- End Article Flow 1 -->
	<section class="ad">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ad-wrap text-center col-md-offset-2">
					<img src="images/ad.jpg" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End Ad -->
	<section id="trending">
		<div class="container">
			<div class="row text-center">
				<h2><i class="btm bt-bar-chart"></i>Trending Articles</h2>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<ul class="articles">
					<li class="article col-md-3"><a href="#">
						<div class="title-wrap">
							<h3>INTERVIEW WITH ISSA CPT ATHLETE MICHAEL WITTIG</h3>
						</div>
						<div class="featured-image-block"></div>
					</a></li>
					<li class="article col-md-3"><a href="#">
						<div class="title-wrap">
							<h3>INTERVIEW WITH ISSA CPT ATHLETE MICHAEL WITTIG</h3>
						</div>
						<div class="featured-image-block" style="background: url('images/trending2.jpg')"></div>
					</a></li>
					<li class="article col-md-3"><a href="#">
						<div class="title-wrap">
							<h3>INTERVIEW WITH ISSA CPT ATHLETE MICHAEL WITTIG</h3>
						</div>
						<div class="featured-image-block" style="background: url('images/trending3.jpg')"></div>
					</a></li>
					<li class="article col-md-3"><a href="#">
						<div class="title-wrap">
							<h3>INTERVIEW WITH ISSA CPT ATHLETE MICHAEL WITTIG</h3>
						</div>
						<div class="featured-image-block" style="background: url('images/trending4.jpg')"></div>
					</a></li>
				</ul>
			</div>
		</div>
	</section>
	<!-- End trending Articles -->
	<section id="article-flow-2">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<ul class="articles">
						<div class="row">
							<li class="col-md-6"><a href="#" style="background: url('images/liftingbg.jpg')">
								<div class="overlay"></div>
								<div class="text-wrap">
								<p><span class="category">Training</span></p>
								<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
								<p class="excerpt">The Stronglifts 5X5 training program has become a well renowned programming method for anyone looking to develop size and strength.</p>
								</div>
							</a></li>
							<li class="col-md-6">
								<ul class="secondary-list articles">
									<div class="row">
										<li><a href="#" style="background: url('images/articlebg2.jpg')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category">Training</span></p>
												<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
											</div>
										</a></li>
									</div>
									<div class="row">
										<li><a href="#" style="background: url('images/articlebg3.jpg')">
											<div class="overlay"></div>
											<div class="text-wrap">
												<p><span class="category">Training</span></p>
												<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
											</div>
										</a></li>
									</div>
								</ul>
							</li>
						</div>
						<div class="row">
							<li class="col-md-4"><a href="#"  style="background: url('images/articlebg1.jpg')">
							<div class="overlay"></div>
								<div class="text-wrap">
								<p><span class="category">Training</span></p>
								<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
								</div>
							</a></li>
							<li class="col-md-4"><a href="#"  style="background: url('images/homebg.jpg')">
							<div class="overlay"></div>
								<div class="text-wrap">
								<p><span class="category">Training</span></p>
								<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
								</div>
							</a></li>
							<li class="col-md-4"><a href="#" style="background: url('images/trending2.jpg')">
							<div class="overlay"></div>
								<div class="text-wrap">
								<p><span class="category">Training</span></p>
								<h2>STRONGLIFTS 5×5 WORKOUT ROUTINE: PROS, CONS &amp; REVIEW</h2>
								</div>
							</a></li>
						</div>
					</ul>
				</div>
			</div>
		</div>
	</section>
	<!-- End article flow 2 -->
	<section class="ad">
		<div class="container">
			<div class="row">
				<div class="col-md-8 ad-wrap text-center col-md-offset-2">
					<img src="images/ad.jpg" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End Ad -->
	<section id="subscribe">
		<div class="container">
			<div class="row">
				<div class="col-md-2">
					<div class="logo-wrap">
						<img src="images/logo.png" alt="">
					</div>
				</div>
				<div class="col-md-6 subscription-form">
					<h3>Subscribe to our newsletter</h3>
					<p>Receive exciting features, news &amp; special offers</p>
					<form action="#" method="post">
						<input type="email" placeholder="Enter e-mail address" name="email" required>
						<button><i class="btm bt-envelope"></i></button>
					</form>
				</div>
			</div>
		</div>
	</section>
	<!-- End Subscribe -->
<?php include 'incl/footer.php'; ?>
















