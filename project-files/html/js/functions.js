// Hamburger Menu
$( ".cross" ).hide();
$( ".menu" ).hide();
$( ".hamburger" ).click(function() {
	$( ".menu" ).slideToggle( "slow", function() {
		$( ".hamburger" ).hide();
		$( ".cross" ).show();
	});
});

$( ".cross" ).click(function() {
	$( ".menu" ).slideToggle( "slow", function() {
		$( ".cross" ).hide();
		$( ".hamburger" ).show();
	});
});

// Search Hide/Show
$('#search-trigger').click(function() {
	if ($('#search-wrap').hasClass('hidden')) {
		$('#search-wrap').removeClass('hidden');
	}
	else {
		$('#search-wrap').addClass('hidden');
	}
});

var $tabs = $('.articles-wrap > ul');
$('.menu-posts a').on('click', function(e) {
	e.preventDefault();
	var currentTab = $(this).attr('href');

	$(this).parent().addClass('active').siblings().removeClass('active');
	$tabs.hide();
	$(currentTab).show();
}).filter(':first').click();

$(function() {
	var handle = $( '.ui-slider-handle' );
	$( '#weight-slider' ).slider({
		max: 600,
		value: 200,
		create: function() {
			handle.html( $( this ).slider( 'value' ) + "<br><span>lbs</span>");
			$('#weight-input').val($( this ).slider( 'value' ));
		},
		slide: function( event, ui ) {
			handle.html( ui.value + "<br><span>lbs</span>" );
			$('#weight-input').val($( this ).slider( 'value' ));
		}
	});
});
$( "#activity" ).selectmenu();


