<?php include 'incl/header.php'; ?>
	<section id="content">
		<div class="container">
			<h2 class="page-title">Training</h2>
			<div class="row">
				<div class="col-md-8">
					<div class="posts-list">
						<article class="post">
							<a href="#" class="image"><img src="images/cat1.jpg" alt=""></a>
							<h4><a href="#">COMBATING SHOULDER INJURY WITH ROTATOR CUFF EXERCISES</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
						<article class="post">
							<a href="#" class="image"><img src="images/cat2.jpg" alt=""></a>
							<h4><a href="#">5 TIPS FOR AN EFFECTIVE PROGRESS JOURNAL</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
						<article class="post">
							<a href="#" class="image"><img src="images/cat3.jpg" alt=""></a>
							<h4><a href="#">COMBATING SHOULDER INJURY WITH ROTATOR CUFF EXERCISES</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
						<article class="post">
							<a href="#" class="image"><img src="images/cat4.jpg" alt=""></a>
							<h4><a href="#">5 TIPS FOR AN EFFECTIVE PROGRESS JOURNAL</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
					</div>
					<div class="post-content-ad">
						<img src="images/ad.jpg" alt="">
					</div>
					<div class="posts-list">
						<article class="post">
							<a href="#" class="image"><img src="images/cat1.jpg" alt=""></a>
							<h4><a href="#">COMBATING SHOULDER INJURY WITH ROTATOR CUFF EXERCISES</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
						<article class="post">
							<a href="#" class="image"><img src="images/cat2.jpg" alt=""></a>
							<h4><a href="#">5 TIPS FOR AN EFFECTIVE PROGRESS JOURNAL</a></h4>
							<p>If you are serious about seeing muscle building results and are someone who is...</p>
						</article>
					</div>
				</div>
				<div class="col-md-4">
					<div class="side-bar">
						<div class="ad">
							<img src="images/gudpod.jpg" alt="">
						</div>
						<div class="newsletter shaped">
							<div class="upper-box">
								<h3>download your <strong>free</strong><span class="larger">supplement</span><span class="large">starter guide</span></h3>
							</div>
							<div class="lower-box">
								<form action="#">
									<input type="email" class="email-input" placeholder="Enter e-mail">
									<button type="submit" class="submit"><i class="btm bt-envelope"></i></button>
								</form>
							</div>
						</div>
						<div class="shares">
							<ul>
								<li>
									<a href="#" class="link-facebook"><i class="fab fab-facebook-alt"></i><span>17K</span></a>
								</li>
								<li>
									<a href="#" class="link-twitter"><i class="fab fab-twitter"></i><span>23K</span></a>
								</li>
								<li>
									<a href="#" class="link-instagram"><i class="fab fab-instagram"></i><span>5K</span></a>
								</li>
							</ul>
						</div>
						<div class="featured-articles">
							<div class="menu-posts">
								<ul>
									<li class="active"><a href="#trending-posts">Trending</a></li>
									<li><a href="#recent-posts">Recent</a></li>
								</ul>
							</div>
							<div class="articles-wrap">
								<ul id="trending-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Zac Efron Workout Routine and Diet</a></p>
										</div>
									</li>
								</ul>
								<ul id="recent-posts">
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-2.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
									<li>
										<div class="article">
											<a href="#"><img src="images/sidebar-img-1.jpg" alt=""></a>
											<p><a href="#">Get ripped in 60 days</a></p>
										</div>
									</li>
								</ul>
							</div>
						</div>
						<div class="facebook-widget">
							<iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fmusclebreak%2F&tabs&width=300&height=154&small_header=true&adapt_container_width=true&hide_cover=false&show_facepile=true&appId" width="300" height="154" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Content -->
<?php include 'incl/footer.php'; ?>