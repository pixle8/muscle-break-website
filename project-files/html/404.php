<?php include 'incl/header.php'; ?>
	<section id="banner-error">
		<div class="container">
			<h2 class="page-title">404</h2>
			<p>trust us, we looked everywhere</p>
		</div>
	</section>
	<!-- Banner Error -->
	
	<section id="content-articles">
		<div class="container">
			<div class="title">
				<h4><em>instead</em> check out our</h4>
				<h3>latest articles</h3>
			</div>

			<ul class="articles-list">
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img1.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">CARB BACKLOADING: WHAT THIS TECHNIQUE IS ALL ABOUT</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img2.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">TRANSFORMATION: HOW RAPPER RICK ROSS LOST 75 POUNDS</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img3.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">WBFF ATHLETE MEISHA PIJOT TALKS WITH MUSCLE BREAK</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img4.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">CARB BACKLOADING: WHAT THIS TECHNIQUE IS ALL ABOUT</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img5.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">TRANSFORMATION: HOW RAPPER RICK ROSS LOST 75 POUNDS</a></h5>
					</div>
				</li>
				<li>
					<div class="item">
						<a href="#" class="image"><img src="images/img6.jpg" alt=""><i class="btm bt-share"></i></a>
						<h5><a href="#">WBFF ATHLETE MEISHA PIJOT TALKS WITH MUSCLE BREAK</a></h5>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<!-- Content Articles -->
<?php include 'incl/footer.php'; ?>