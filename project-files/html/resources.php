<?php include 'incl/header.php'; ?>
	<section id="resources-banner">
		<div class="container">
			<h2 class="page-title">Resources</h2>
		</div>
	</section>
	<!-- End About Banner -->

	<section id="resources">
		<div class="container">
			<div class="boxes">
				<div class="box">
					<div class="box-title">
						<img src="images/res-1.jpg" alt="">
						<h2>BMR <small>Calculator</small></h2>
					</div>
					<div class="box-body">
						<div class="text">
							<p>Your <strong>Basal Metabolic Rate</strong> is essentially the sum of all the energy your body uses each day to simply stay alive. Note this is not the number of calories needed to get you out of bed in the morning, to brush your teeth, or to cook  breakfast.</p>
						</div>
						<a href="#" class="btn">Launch</a>
					</div>
				</div>
				<div class="box">
					<div class="box-title">
						<img src="images/res-2.jpg" alt="">
						<h2>BMI <small>Calculator</small></h2>
					</div>
					<div class="box-body">
						<div class="text">
							<p>Your <strong>Body Mass Index</strong> is a measure of body fat based on height and weight that applies to adult men and women. This will give you a very rough estimation of your relative health.</p>
						</div>
						<a href="#" class="btn">Launch</a>
					</div>
				</div>
				<div class="box">
					<div class="box-title">
						<img src="images/res-3.jpg" alt="">
						<h2>1RM <small>Calculator</small></h2>
					</div>
					<div class="box-body">
						<div class="text">
							<p>Use the calculator below for any lift to estimate your <strong>One-Rep Max</strong> based on the amount of weight you can lift on a given move, and the number of clean reps you can achieve before muscle failure. </p>
						</div>
						<a href="#" class="btn">Launch</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- End Resources -->

	<section id="subscribe-ebook">
		<div class="container">
			<div class="box-wrap">
				<div class="form-wrap">
					<h3>subscribe to our newsletter<br><strong>get your free supplement guide</strong></h3>
					<form action="" method="post">
						<input type="email" placeholder="Enter e-mail address" name="email" required>
						<button type="submit"><i class="btm bt-envelope"></i></button>
					</form>
				</div>
				<div class="ebook-image">
					<img src="images/ebook-placeholder.jpg" alt="">
				</div>
			</div>
		</div>
	</section>
	<!-- End Subscribe -->
	
<?php include 'incl/footer.php'; ?>