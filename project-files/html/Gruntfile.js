module.exports = function(grunt) {
    grunt.initConfig({
        sass: { // Task
            dist: {  // Target
                options: { // Target options
                    style: 'compressed'
                },

                files: { // Dictionary of files
                    'css/styles.css': 'css/styles.scss', // 'destination': 'source'
                }
            }
        },

        postcss: {
            options: {
                map: true,
                processors: [
                    require('autoprefixer')({browsers: ['last 3 versions']})
                ]
            },
            dist: {
                src: 'css/style.css'
            }
        },

        watch: {
            styles: {
                files: '**/*.scss',
                tasks: ['sass'],
                options: {
                    debounceDelay: 250,
                },
            },
        }
    });

    // grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-postcss');
    grunt.loadNpmTasks('grunt-browser-sync');

    grunt.registerTask('default', ['sass', 'postcss', 'watch']);
};
